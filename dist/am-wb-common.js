/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-common', [ 
	'am-wb-core',
	'youtube-embed' //https://github.com/brandly/angular-youtube-embed
	]);

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('am-wb-common')
.filter('trueOrUndefined', function() {
	return function(value) {
		var res = (value === true || value === 'true');
		return res ? res : undefined;
	};
});


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/*
 * 
 */
angular.module('am-wb-common')
/*
 * 
 */
.config(['wbIconServiceProvider', function(wbIconServiceProvider) {
	wbIconServiceProvider
	// Move actions
	.addShape('wb-common-link', wbIconServiceProvider.getShape('link'))
	.addShape('wb-common-toolbar', wbIconServiceProvider.getShape('more_horiz'))
	.addShape('wb-common-video', wbIconServiceProvider.getShape('video_library'))
	.addShape('wb-common-audio', wbIconServiceProvider.getShape('audiotrack'))

	.addShape('wb-common-aparat', '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="m 153.79381,10.305425 c -6.1899,0.01113 -10.39332,0.456735 -13.86367,1.43555 -3.55933,0.949155 -11.03508,4.033558 -16.61135,6.762378 C 109.6747,25.147434 96.031412,38.554008 89.743261,51.486241 86.183932,58.6049 78.472636,84.232185 73.133643,106.18138 c -0.593225,2.25425 3.796149,-1.54249 16.13516,-13.762853 24.440727,-24.440729 47.693857,-39.98255 77.710857,-52.202919 13.40681,-5.457638 39.62779,-12.81486 51.01764,-14.357235 3.44067,-0.474578 6.05121,-1.067915 5.69527,-1.305203 -0.2373,-0.23729 -11.39078,-3.440686 -24.67894,-7.118659 -21.59327,-5.813571 -25.62695,-6.524162 -38.32188,-6.998739 -2.55086,-0.08899 -4.83464,-0.134055 -6.89794,-0.130347 z M 255.67266,42.33417 c -6.22629,0.05214 -12.46287,0.37308 -18.69169,0.966304 -24.67803,2.37289 -48.99987,9.253682 -72.13551,20.168961 C 99.591089,94.554236 55.098895,154.35097 44.420908,225.41891 c -2.254248,15.18647 -2.254248,46.27105 0,61.45751 10.677987,71.06794 55.170181,130.86468 120.424552,161.94948 69.40692,32.98313 150.2031,27.05044 214.15239,-15.89879 48.16959,-32.27125 79.72921,-80.20356 91.59363,-138.81384 3.08476,-15.30511 3.08476,-60.62609 0,-75.93121 C 458.72706,159.57178 427.16744,111.63948 378.99785,79.368229 342.35159,54.764366 299.2566,41.969206 255.67266,42.33417 Z m 145.9012,28.940412 c -0.40412,-0.01851 -0.62739,0.02562 -0.62739,0.14425 0,0.474575 2.37288,2.609015 5.339,4.744612 13.52544,10.203414 34.28935,33.102106 46.86564,51.966556 17.20344,25.50852 30.72889,59.55909 35.5933,90.40661 0.83051,4.74578 1.6601,8.42479 2.01602,8.1875 0.2373,-0.35593 3.91631,-13.52522 8.1875,-29.42355 6.64409,-25.38989 7.59312,-30.25418 7.59312,-39.62709 0,-21.00004 -7.71245,-39.3906 -22.54299,-54.10249 C 472.01499,91.825187 461.337,86.486309 435.94711,79.960875 c -10.44069,-2.728822 -22.54322,-5.932218 -26.93306,-7.118658 -3.38136,-0.889833 -6.22779,-1.512022 -7.44019,-1.567635 z M 195.66807,100.10203 c 4.3732,-0.0411 8.82277,0.39867 13.31272,1.33299 33.33905,6.88138 55.05131,41.40792 46.62755,74.0351 -11.86442,45.55942 -69.17067,61.10112 -102.50972,27.76207 -23.96615,-23.96615 -23.84715,-60.62782 0.35629,-84.83125 11.95712,-11.86443 26.59456,-18.1521 42.21316,-18.29891 z m 156.68521,29.8059 c 4.46723,0.0924 10.13794,0.83271 13.35618,1.66322 27.52548,7.11866 45.915,30.37224 45.915,58.01637 0,17.91528 -5.10056,30.73014 -17.20227,42.83186 -8.30511,8.30511 -18.39056,13.99946 -29.1872,16.60963 -14.94918,3.44069 -27.52606,1.89831 -42.1193,-5.339 -23.72886,-11.62714 -37.37249,-38.91486 -32.15215,-64.1861 2.72882,-13.28817 7.23754,-21.71249 16.84773,-31.44132 12.45765,-12.45765 25.27123,-17.91564 42.71195,-18.15293 0.57839,-0.0148 1.19188,-0.0149 1.83006,-0.002 z m -95.98544,101.12283 c 4.08835,-0.0482 8.3467,0.89053 12.52891,3.04838 9.01696,4.62712 14.59185,13.28954 14.59185,22.78109 0,20.52547 -19.93108,33.1006 -38.55824,24.32093 -7.00001,-3.2034 -9.13607,-5.45718 -12.33946,-12.57584 -8.48308,-18.6049 6.06069,-37.36595 23.77694,-37.57456 z m -90.82025,30.61753 c 3.74298,0.003 7.51781,0.33948 11.28105,1.02539 3.55933,0.59323 10.79664,3.32147 16.01698,5.93164 23.72887,11.62714 37.37251,38.91487 32.15215,64.1861 -2.72881,13.28817 -7.23754,21.71249 -16.84773,31.44133 -19.45766,19.57631 -46.62755,23.84692 -71.54285,11.27063 -31.91533,-16.13563 -42.473621,-58.01695 -22.06681,-87.67803 11.5122,-16.51751 30.79515,-26.19349 51.00721,-26.17706 z m 153.65248,29.51045 c 5.28153,-0.063 10.58994,0.55268 15.78062,1.88743 31.20345,8.0678 51.01706,39.50867 44.49162,70.71211 -3.44068,16.25428 -17.55982,34.76175 -31.91578,41.64312 -24.55938,11.86443 -52.44044,7.47551 -70.8303,-10.91436 -24.0848,-23.96614 -24.2038,-61.69456 -0.35628,-85.54208 11.38985,-11.30087 26.98555,-17.59713 42.83012,-17.78622 z m -296.583532,1.94651 c -0.09542,0.007 -0.144252,0.26 -0.144252,0.77165 0,0.47458 -2.966683,11.86432 -6.526012,25.38977 -5.576285,21.23733 -6.5260213,26.10094 -6.5260213,35.35519 0,21.00005 7.7129233,39.39107 22.4248153,53.98431 12.694944,12.45766 21.237102,16.61021 52.559199,24.91531 13.881384,3.67798 25.508763,6.52475 25.746053,6.4061 0.23729,-0.23728 -1.06838,-1.66113 -2.96669,-3.08487 -6.64408,-5.1017 -28.118357,-26.5755 -35.237015,-35.35519 C 50.234707,374.79255 33.387566,339.43644 26.031616,305.8601 24.296442,297.66622 23.029998,293.07513 22.616538,293.10525 Z M 444.42833,397.6032 c -0.62613,0.10217 -2.63798,2.28274 -4.92188,5.18953 -2.61018,3.44068 -9.6103,11.1514 -15.54253,17.20226 -32.27124,32.3899 -74.62773,55.40678 -119.11934,64.54239 -6.76272,1.30509 -12.57492,2.72939 -12.8122,2.96669 -0.47458,0.47457 -0.59461,0.47526 27.04952,7.83121 36.4238,9.72883 51.13604,9.49016 73.32253,-1.42512 16.37292,-8.06781 30.96627,-23.60963 36.89848,-39.74526 3.08476,-7.94917 15.77901,-55.05107 15.30443,-56.4748 -0.0296,-0.0741 -0.0895,-0.10154 -0.17901,-0.0869 z" /></svg>')
	.addShape('wb-common-youtube', '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="m 511.339,212.987 c -0.186,-10.277 -1,-23.271 -2.423,-38.97 -1.431,-15.708 -3.478,-29.746 -6.14,-42.115 -3.046,-13.893 -9.661,-25.6 -19.842,-35.117 C 472.753,87.266 460.903,81.748 447.385,80.223 405.127,75.468 341.27,73.088 255.812,73.088 170.353,73.088 106.495,75.468 64.24,80.223 50.724,81.747 38.921,87.266 28.836,96.785 18.747,106.299 12.18,118.006 9.134,131.902 6.282,144.275 4.138,158.312 2.711,174.017 1.286,189.716 0.476,202.705 0.287,212.987 0.094,223.265 0,237.539 0,255.813 c 0,18.272 0.094,32.55 0.288,42.826 0.189,10.284 0.999,23.271 2.424,38.969 1.427,15.707 3.474,29.745 6.139,42.116 3.046,13.897 9.659,25.602 19.842,35.115 10.185,9.517 22.036,15.036 35.548,16.56 42.255,4.76 106.109,7.139 191.572,7.139 85.466,0 149.315,-2.379 191.573,-7.139 13.518,-1.523 25.316,-7.043 35.405,-16.56 10.089,-9.514 16.652,-21.225 19.698,-35.115 2.854,-12.371 4.996,-26.409 6.427,-42.116 1.423,-15.697 2.231,-28.691 2.423,-38.969 0.191,-10.276 0.287,-24.554 0.287,-42.826 0,-18.274 -0.095,-32.548 -0.287,-42.826 z M 356.883,271.231 210.706,362.59 c -2.666,1.903 -5.905,2.854 -9.71,2.854 -2.853,0 -5.803,-0.764 -8.848,-2.286 -6.28,-3.422 -9.419,-8.754 -9.419,-15.985 V 164.454 c 0,-7.229 3.14,-12.561 9.419,-15.986 6.473,-3.431 12.657,-3.239 18.558,0.571 l 146.178,91.36 c 5.708,3.23 8.562,8.372 8.562,15.415 0,7.04 -2.855,12.184 -8.563,15.417 z" /></svg>')
	
	.addShape('wb-common-moveup', '<path d="M7.41,15.41L12,10.83L16.59,15.41L18,14L12,8L6,14L7.41,15.41Z" />')
	.addShape('wb-common-movedown', '<path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" />')
	;
}]);



/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
   * @ngdoc Controllers
   * @name AmWbCommonAudioCtrl
   * @description control the Audio Player widget.
   */
angular.module('am-wb-common')
/**
 * 
 */
.controller('AmWbCommonAudioCtrl', function($scope) {
    // XXX: maso, 2019: Update based on the new model
	var ngModel = $scope.wbModel;

	/*
	 * Listen model
	 */
	$scope.$watch('wbModel',  function() {
		if (angular.isDefined($scope.wbModel)) {
			ngModel = $scope.wbModel;
			if (!angular.isDefined(ngModel.file)) {
				ngModel.content = {};
				ngModel.content.url = 'https://www.w3schools.com/html/mov_bbb.ogg';
				ngModel.content.mimeType = 'audio/mpeg';
				ngModel.setting = {};
				ngModel.setting.autoplay = 'false';
				ngModel.setting.controls = 'true';
				ngModel.setting.preload = 'auto';
			}
		}
	});
	// Global functions
});
'use strict';

angular.module('am-wb-common')
/**
 * @ngdoc function
 * @name AmWbCommonDialogmodelCtrl
 * @description # Dialog controller
 * 
 * This is common dialog controller.
 */
.controller('AmWbCommonDialogmodelCtrl', function($scope, $mdDialog, style, model) {
	$scope.model = model;
	$scope.style = style;
	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
		$mdDialog.cancel();
	};
	$scope.answer = function(a) {
		$mdDialog.hide(a);
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('am-wb-common')
/**
 * @ngdoc Controllers
 * @name AmWbCommonFeaturesCtrl
 * @description control some general properties of different widgets.
 * 
 * The controller is used with widgets
 */
.controller('AmWbCommonFeaturesCtrl', function($scope, $wbUi, $window, $resource,
        $anchorScroll, $location) {
    var ngModel = $scope.wbModel;

    function addFeature() {//
        // first version of model
        var feature =  {
                'icon' : 'assignment_turned_in',
                'title' : 'Item title',
                'text' : 'Item description.',
                action : {
                    type : 'link',
                    link : ''
                }
        };
        /*
         * 
         * this function uses an internal function named
         * $mdDialog.show. $mdDialog.show shows a dialog box
         * using an input data and waits until the dialog box be
         * closed. Then it returns a new version of data. So, if
         * we want to do some works over output of dialog box,
         * we need to do this works inside
         * '.then(function(newData))' which comes just next to
         * the calling $mdDialog.show function. if we don't want
         * to do work over output (want the dialog service just
         * to change the input data and replace the old version)
         * we don't need to .then part). the function
         * editFeature() comes later in this controller works
         * this way.
         */
        $wbUi.openDialog(
                {
                    controller : 'AmWbCommonDialogmodelCtrl',
                    templateUrl : 'views/am-wb-common-dialogs/feature.html',
                    parent : angular
                    .element(document.body),
                    clickOutsideToClose : true,
                    locals : {
                        model : feature,
                        style : {
                            title : 'service'
                        }
                    }
                }).then(function(newFeature) {
                    // Example of how $mdDialog.show return a
                    // new version of its input:
                    // https://stackoverflow.com/questions/41650984/angularjs-mddialog-how-to-send-back-data-to-my-first-controller
                    ngModel.features.push({
                        'icon' : newFeature.icon,
                        'title' : newFeature.title,
                        'text' : newFeature.text,
                        action : {
                            type : newFeature.action.type,
                            link : newFeature.action.link
                        }
                    });
                });
    }

    function removeFeature(feature) {
        var index = ngModel.features.indexOf(feature);
        if (index > -1) {
            ngModel.features.splice(index, 1);
        }
    }

    function removeAllFeatures() {
        ngModel.features = [];
    }

    function editFeature(feature) {
        $wbUi.openDialog({
            controller : 'AmWbCommonDialogmodelCtrl',
            templateUrl : 'views/am-wb-common-dialogs/feature.html',
            parent : angular.element(document.body),
            clickOutsideToClose : true,
            locals : {
                model : feature,
                style : {
                    title : 'service'
                }
            }
        });
    }

    function goToAnchor(anchor) {
        if ($location.hash() !== anchor) {
            // set the $location.hash to `anchor` and
            // $anchorScroll will automatically scroll to it
            $location.hash(anchor);
        } else {
            // call $anchorScroll() explicitly,
            // since $location.hash hasn't changed
            $anchorScroll();
        }
    }

    function moveArrayItem(array, old_index, new_index) {
        if (new_index >= array.length) {
            var k = new_index - array.length;
            while ((k--) + 1) {
                array.push(undefined);
            }
        }
        array.splice(new_index, 0,
                array.splice(old_index, 1)[0]);
    }

    function moveUpFeature(feature) {
        var index = ngModel.features.indexOf(feature);
        if (index === 0) {
            return;
        }
        moveArrayItem(ngModel.features, index, index - 1);
    }

    function moveDownFeature(feature) {
        var index = ngModel.features.indexOf(feature);
        if (index === ngModel.features.length - 1) {
            return;
        }
        moveArrayItem(ngModel.features, index, index + 1);
    }

    /*
     * run feature action
     */
    function runAction(feature, params, event) {
        // TODO: support page parameters
        var link = feature.action.link;
        var type = feature.action.type;
        if (!$scope.ctrl.isEditable()) {
            // Ex. 'https://www.google.com'
            if (link.toLowerCase().startsWith('http')) {
                $window.open(link);
            }
            if (type === 'link') {
                if (params) {
                    $location.path(link).search(params);
                } else {// page in current spa-> Ex.
                    // 'welcome-page'
                    $location.path(link);
                }
            } else {// if(type === 'internal-link')a link in
                // current
                // page
                goToAnchor(link);
            }
        }
        if (event){
            event.preventDefault();
        }
    }


    /**
     * Selects featrue image
     * 
     * @param feature
     * @returns
     */
    function selectImage(feature) {
        return $resource.get('image')//
        .then(function(value) {
            feature.image = value;
        });
    }

    $scope.extraActions = [
        {
            title : 'Add feature',
            icon : 'add',
            action : addFeature
        },
        {
            title : 'Remove feature',
            icon : 'remove',
            action : function() {
                removeFeature(ngModel.features[ngModel.features.length - 1]);
            }
        },
        {
            title : 'Toggle layout',
            icon : 'swap_vert',
            action : function() {
                ngModel.layout = 'column';
            },
            disable : function() {
                return ngModel.layout === 'column';
            }
        },
        {
            title : 'Toggle layout',
            icon : 'swap_horiz',
            action : function() {
                ngModel.layout = 'row';
            },
            disable : function() {
                return (!ngModel.layout) || ngModel.layout === 'row';
            }
        } ];

    /*
     * Listen model
     */
    $scope.$watch('wbModel', function() {
        if (angular.isDefined($scope.wbModel)) {
            ngModel = $scope.wbModel;
            if (!angular.isDefined(ngModel.features)) {
                ngModel.features = [];
            }
        }
    });

    // Global functions
    $scope.addFeature = addFeature;
    $scope.removeFeature = removeFeature;
    $scope.removeAllFeatures = removeAllFeatures;
    $scope.editFeature = editFeature;
    $scope.moveUpFeature = moveUpFeature;
    $scope.moveDownFeature = moveDownFeature;
    $scope.runAction = runAction;
    $scope.goToAnchor = goToAnchor;
    $scope.selectImage = selectImage;

    this.test = 'hi';
    this.addFeature = addFeature;
    this.runAction = runAction;

});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * @ngdoc Controllers
 * @name AmWbCommonNavBarCtrl
 * @description control the NavBar widget.
 */
angular.module('am-wb-common')

.controller('AmWbCommonNavBarCtrl', function () {
	// Check data model
	if(!angular.isArray( $scope.wbModel.tabs)){
		$scope.wbModel.tabs = [];
	}
	var tabs = $scope.wbModel.tabs;

	function deleteTab(index) {
	    $scope.wbModel.tabs.splice(index, 1);
	}

	function addTab() {
		var tab = {
				'type': 'Group',
				'label': 'Title',
				'contents': []
		};
		tabs.push(tab);
	}

	this.deleteTab = deleteTab;
	this.addTab = addTab;
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('am-wb-common')
/**
 * @ngdoc Controllers
 * @name AmWbCommonVideoCtrl
 * @description control the Video widget.
 * 
 * All properties will fetch from the Model such as url.
 */
.controller('AmWbCommonVideoCtrl', function($scope) {
    var keys = ['fit', 'url', 'mimeType', 'autoplay', 'loop', 'controls', 'preload', 'poster', 'description', 'label', 'keywords'];

    /**
     * Loads all parameters from model
     * 
     * @memberof AmWbCommonVideoCtrl
     */
    this.fillMedia = function() {
        for(var i = 0; i < keys.length; i++){
            var key = keys[i];
            this[key] = this.getModelProperty(key);
        }
    };


    /**
     * Initialize the widget
     * 
     * @memberof AmWbCommonVideoCtrl
     */
    this.initWidget = function(){
        var ctrl = this;
        this.on('modelUpdated', function($event){
            if(keys.indexOf($event.key) > -1){
                var key = $event.key;
                ctrl[key] = ctrl.getModelProperty(key);
            }
        });
        this.on('modelChanged', function(){
            ctrl.fillMedia();
        });
        this.fillMedia();
    };
});
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
/**
 * @ngdoc Controllers
 * @name WelcomeCtrl
 */
angular.module('am-wb-common')
/**
 * 
 */
.controller('WelcomeCtrl', function () {

});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-common')

/**
 * @ngdoc Directives
 * @name am-wb-feature-button
 * @description Show a feature in button with action
 * 
 * This is used in toolbar
 */
.directive('amWbFeatureButton', function($q, $parse, $resource, $anchorScroll, $location) {

	/**
	 * Link data and view
	 */
	function postLink(scope, attr, elem, ngModel) {
		var ctrl = {};
		
		function goToAnchor(anchor){
			if ($location.hash() !== anchor) {
				// set the $location.hash to `anchor` and
				// $anchorScroll will automatically scroll to it
				$location.hash(anchor);
			} else {
				// call $anchorScroll() explicitly,
				// since $location.hash hasn't changed
				$anchorScroll();
			}
		}

		/*
		 * Load the feature
		 */
		function loadFeatrue(feature){
			scope.feature = feature;
			if(!feature.action){
				return;
			}
			switch(feature.action.type) {
			case 'link':
			case 'internal-link':
				ctrl.type = feature.action.type;
				ctrl.link = feature.action.link;
				break;
			default:
				ctrl.type = 'unknown';
			}
		}

		/*
		 * Run the current feature
		 */
		scope.runAction = function(){
			// TODO: maso, 2018: handle other types
			if(ctrl.type === 'internal-link'){
				goToAnchor(ctrl.link);
			}
		};
		
		// Watch model
		scope.$watch(function(){
			return ngModel.$modelValue;
		}, loadFeatrue, true);
		scope.featue = ngModel.$viewValue;
		scope.ctrl = ctrl;
	}

	return {
		restrict : 'E',
		transclude : true,
		replace: true,
		require: '^ngModel',
		scope: {
			wbIconButton: '<?'
		},
		templateUrl : 'views/directives/am-wb-feature-button.html',
		link: postLink,
		controller: function ($scope, $rootScope) {
		    $rootScope.$watch('editable', function (val) {
			$scope.editable = val;
		    });
		}
	};
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-common')

/**
 * @ngdoc Directives
 * @name wbPreventReload
 * @param {element}
 *            element
 * @description prevent the element to do a default/predefined work on its
 *              click. This diretive: - Acts as an attribute - Get an element as
 *              input and define a function on its 'click'. - Disables the
 *              default functionality of 'element' and allows us to handle it in
 *              other ways.
 * 
 * Why is this directive important? Example:
 * 
 * Sometimes we need to both 'ng-href'(for better performance when the search
 * engines are searching) and 'ng-click'(to do other thing)in a button's
 * attributes . But we want to disable 'ng-href' and allow the 'ng-click' to
 * handle the situation. If we use 'ng-href' for example in a 'Button' to go to
 * a link and click on the button then the explorer will go to that link
 * automatically. Now, if we want to prevent explorer to do this, we could use
 * this directive as 'attribute' in button's attributes and handle the button
 * 'onClick' in another way.
 * 
 * @author masood<masoodzarei64@gamil.com>
 */
.directive('wbPreventReload', function() {
	return {
		restrict : 'A',
		link : function(scope, element) {
			element.on('click', function(event) {
				event.preventDefault();
			});
		}
	};
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-common').directive('amWbSettingAparat', function() {
    return {
        templateUrl : 'views/directives/am-wb-setting-aparat.html',
        restrict : 'E',
        scope : {
            title : '@title', // or '@'
            lable : '@lable', // or '@'
            icon : '@icon' // or '@'
        },
        require: ['ngModel'],
        link:  function (scope, element, attr, ctrls) {
            var ngModelCtrl = ctrls[0];

            ngModelCtrl.$render = function () {
                scope.value = ngModelCtrl.$modelValue;
            };

            scope.valueChanged = function (value) {
                ngModelCtrl.$setViewValue(value);
            };
        }
    };
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-common')
.directive('amWbSettingYoutube', function() {
    return {
        templateUrl : 'views/directives/am-wb-setting-youtube.html',
        restrict : 'E',
        scope : {
            title : '@title', // or '@'
            lable : '@lable', // or '@'
            icon : '@icon' // or '@'
        },
        require: ['ngModel'],
        link:  function (scope, element, attr, ctrls) {
            var ngModelCtrl = ctrls[0];

            ngModelCtrl.$render = function () {
                scope.value = ngModelCtrl.$modelValue;
            };

            scope.valueChanged = function (value) {
                ngModelCtrl.$setViewValue(value);
            };
        }
    };
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('am-wb-common')

/**
 * Load widgets
 */
.run(function ($settings, $wbUi) {

    /*
     * Utility: change two indexs
     */
    function moveArrayItem(array, old_index, new_index) {
        if (new_index >= array.length) {
            var k = new_index - array.length;
            while ((k--) + 1) {
                array.push(undefined);
            }
        }
        array.splice(new_index, 0,
                array.splice(old_index, 1)[0]);
    }


    /**
     * @ngDoc Settings
     * @name common-features
     * @description Common feature list configurations
     */
    $settings.newPage({
        type: 'common-features',
        label: 'Items',
        icon: 'settings',
        templateUrl: 'views/am-wb-common-settings/features.html',
        description: 'Manage list of feature items',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            /*
             * Load data
             */
            this.init = function () {
                this.features = this.getProperty('features') || [];
            };
            this.removeFeature = function (index) {
                this.features.splice(index, 1);
                this.changedFeature();
            };
            this.moveUpFeature = function (index) {
                moveArrayItem(this.features, index, index - 1);
            };
            this.moveDownFeature = function (index) {
                moveArrayItem(this.features, index, index + 1);
            };
            this.addFeature = function () {
                this.features.push({
                    'icon': 'assignment_turned_in',
                    'title': 'Title',
                    'text': 'Item description.',
                    action: {
                        type: 'link',
                        link: ''
                    }
                });
                this.changedFeature();
            };
            this.editFeature = function (feature) {
                var ctrl = this;
                $wbUi.openDialog({
                    controller: 'AmWbCommonDialogmodelCtrl',
                    templateUrl: 'views/am-wb-common-dialogs/feature.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    locals: {
                        model: feature,
                        style: {
                            title: 'fetrues'
                        }
                    }
                })
                .then(function () {
                    ctrl.changedFeature();
                });
            };
            /*
             * sets new features
             */
            this.changedFeature = function () {
                this.setProperty('features', this.features);
            };
        }
    });

    /**
     * @ngDoc Settings
     * @name common-audio-player
     * @description Audio player setting page
     */
    $settings.newPage({
        type: 'common-audio-player',
        label: 'Audio Player',
        description: 'Manage playing audio in the current widget.',
        icon: 'settings',
        templateUrl: 'views/am-wb-common-settings/audio-player.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            /*
             * Load from model on model change
             */
            this.init = function () {
                this.content = this.getProperty('content') || {};
                this.setting = this.getProperty('setting') || {};
            };

            this.contentChanged = function () {
                this.setProperty('content', this.content);
            };

            this.settingChanged = function () {
                this.setProperty('setting', this.setting);
            };
        }

    });

    /**
     * @ngDoc Settings
     * @name common-video-player
     * @description Video player setting page
     */
    $settings.newPage({
        type: 'common-video-player',
        label: 'Video Player',
        description: 'Manage showing video in the current widget.',
        icon: 'settings',
        templateUrl: 'views/am-wb-common-settings/video-player.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            /*
             * Load from model on model change
             */
            this.init = function () {
                this.type = this.getProperty('type');
                this.url = this.getProperty('url');
                this.mimeType = this.getProperty('mimeType');

                this.preload = this.getProperty('preload');
                this.poster = this.getProperty('poster');
                this.fit = this.getProperty('fit');

                this.autoplay = this.getProperty('autoplay');
                this.controls = this.getProperty('controls');
                this.loop = this.getProperty('loop');
            };

            this.updateFit = function () {
                this.setProperty('fit', this.fit);
            };
            this.updatePoster = function () {
                this.setProperty('poster', this.poster);
            };
            this.updatePreload = function () {
                this.setProperty('preload', this.preload);
            };

            this.updateMimeType = function () {
                this.setProperty('mimeType', this.mimeType);
            };
            this.updateUrl = function () {
                this.setProperty('url', this.url);
            };

            this.updateAutoplay = function () {
                this.setProperty('autoplay', this.autoplay);
            };
            this.updateControls = function () {
                this.setProperty('controls', this.controls);
            };
            this.updateLoop = function () {
                this.setProperty('loop', this.loop);
            };
        }
    });

    /**
     * @ngDoc Settings
     * @name amh-common-image
     * @description Image setting page
     */
    $settings.newPage({
        type: 'amh-common-image',
        label: 'Image',
        description: 'Manage showing image in the current widget.',
        icon: 'settings',
        templateUrl: 'views/am-wb-common-settings/image.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            /*
             * Load from model on model change
             */
            this.init = function () {
                this.url = this.getProperty('url');
                this.fit = this.getProperty('fit');
            };

            this.updateUrl = function () {
                this.setProperty('url', this.url);
            };
            this.updateFit = function () {
                this.setProperty('fit', this.fit);
            };
        }
    });

    /**
     * @ngDoc Settings
     * @name nav-bar
     * @description Link setting page
     */
    $settings.newPage({
        type: 'nav-bar',
        label: 'Navigation bar',
        description: 'Navigation through tabs.',
        icon: 'subject',
        templateUrl: 'views/am-wb-common-settings/navigation-bar.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            // Check data model
            this.init = function () {
                this.tabs = this.getProperty('tabs') || [];
            };

            this.deleteTab = function (index) {
                this.tabs.splice(index, 1);
                this.tabsChanged();
            };

            this.addTab = function () {
                this.tabs.push({
                    'type': 'Group',
                    'label': 'Title',
                    'contents': []
                });
                this.tabsChanged();
            };

            this.tabsChanged = function () {
                this.setProperty('tabs', this.tabs);
            };
        }
    });

    /**
     * @ngdoc Widget setting
     * @name shape
     * @description Manages shape settings
     */
    $settings.newPage({
        type: 'shape',
        label: 'Shape',
        description: 'Manage shape settings.',
        icon: 'format_shapes',
        templateUrl: 'views/am-wb-common-settings/shape.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            this.init = function () {
                this.fill = this.getStyle('fill');
                this.stroke = this.getStyle('stroke');
                this.strokeWidth = this.getStyle('strokeWidth');
                this.content = this.getProperty('content');
            };

            this.styleChanged = function (key, value) {
                this.setStyle(key, value);
            };

            this.contentChanged = function (content) {
                this.setProperty('content', content);
            };
        }
    });

    /**
     * @ngdoc Widget setting
     * @name import
     * @description Import setting
     */
    $settings.newPage({
        type: 'import',
        label: 'Import',
        description: 'Settings of import.',
        icon: 'format_shapes',
        templateUrl: 'views/am-wb-common-settings/import.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function ($cms) {
            this.init = function () {
                this.url = this.getProperty('url');
            };

            this.urlChanged = function () {
                this.setProperty('url', this.url);
                this.checkForCanonicalLink(this.url);
            };

            /*
             * Check the content which the imported widget comes from 
             * to see if the content has had a canonical link. If so, it 
             * set canonicalLink in view.
             */
            this.checkForCanonicalLink = function (url) {
                if (this.canonicalLink) {
                    this.canonicalLink = null;
                }
                var ctrl = this;
                var res = url.match('/api\/v2\/cms\/contents\/([^/]+)\/content#.*');
                if (res) {
                    //res[1] is equal to every thing inside ([^/]+) which is the 'id' of content
                    this.id = res[1];
                    $cms.getContent(this.id, {'graphql': '{metas{key,value}}'})
                    .then(function (res) {
                        for (var i = 0; i < res.metas.length; i++) {
                            if (res.metas[i].key === 'link.canonical') {
                                ctrl.canonicalLink = res.metas[i].value;
                            }
                        }
                    }, function () {
                        //TODO: Masood, 2019: handle error
                    });
                } 
            };
        }
    });
});



/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('am-wb-common')
/*
 * Register modules
 */
.run(function ($widget) {
	// NOTE: audio is moved into core module
	// NOTE: video is moved into core module
	// NOTE: image is move to core module => img

	/*
	 * Default data model of featrues
	 */
	var featuresModel = {
			text: 'Header text',
			features: [{
				title: 'Title',
				text: 'This is a sample link to a site',
				icon: 'todo',
				action: {
					type: 'link',
					link: 'http://google.com'
				}
			}, {
				title: 'Title',
				text: 'This is a sample link to a site',
				icon: 'todo',
				action: {
					type: 'link',
					link: 'http://google.com'
				}
			}]
	};
	$widget.newWidget({
		type: 'CommonFeatureList',
		title: 'Feature list',
		description: 'Horizontal or vertical features list which is used to diaplay common functions of a business.',
		groups: ['commons'],
		icon: 'wb-common-link',
		// help
		help: 'https://gitlab.com/am-wb/am-wb-common/wikis/home',
		helpId: '',
		model: angular.merge({}, featuresModel),
		// functional (page)
		templateUrl: 'views/am-wb-common-widgets/feature-list.html',
		controller: 'AmWbCommonFeaturesCtrl',
		controllerAs: 'featuresCtrl',
		setting: ['common-features']
	});
	$widget.newWidget({
		type: 'CommonFeatureLinks',
		title: 'Link list',
		description: 'List of links and actions to display in row or column',
		groups: ['commons'],
		icon: 'wb-common-link',
		// help
		help: 'http://dpq.co.ir',
		helpId: '',
		model: angular.merge({}, featuresModel),
		// functional
		templateUrl: 'views/am-wb-common-widgets/feature-links.html',
		controller: 'AmWbCommonFeaturesCtrl',
		controllerAs: 'featuresCtrl',
		setting: ['common-features']
	});
	$widget.newWidget({
		// widget
		type: 'CommonFeatureMozaic',
		title: 'Mosaic list',
		description: 'Mosaic view of features',
		groups: ['commons'],
		icon: 'apps',
		// help
		help: 'http://dpq.co.ir',
		helpId: '',
		model: angular.merge({}, featuresModel),
		// functional
		templateUrl: 'views/am-wb-common-widgets/feature-mozaic.html',
		controller: 'AmWbCommonFeaturesCtrl',
		controllerAs: 'featuresCtrl',
		setting: ['common-features']
	});
	$widget.newWidget({
		// widget
		type: 'CommonActionCall',
		title: 'Action call',
		description: 'Call action with brand, title and text.',
		groups: ['commons'],
		icon: 'wb-common-link',
		// help 
		help: 'http://dpq.co.ir',
		helpId: '',
		model: angular.merge({}, featuresModel),
		// page
		templateUrl: 'views/am-wb-common-widgets/action-call.html',
		controller: 'AmWbCommonFeaturesCtrl',
		controllerAs: 'featuresCtrl',
		setting: ['SEO', 'common-features']
	});
	$widget.newWidget({
		// widget
		type: 'CommonFeatureToolbar',
		title: 'Toolbar',
		description: 'A toolbar to show actions with sidenav.',
		groups: ['commons'],
		icon: 'wb-common-toolbar',
		// help
		help: 'https://gitlab.com/weburger/am-wb-common/wikis/toolbar',
		helpId: '',
		model: angular.merge({}, featuresModel),
		// page
		templateUrl: 'views/am-wb-common-widgets/feature-toolbar.html',
		controller: 'AmWbCommonFeaturesCtrl',
		controllerAs: 'featuresCtrl',
		setting: ['SEO', 'common-features']
	});
	$widget.newWidget({
		// widget
		type: 'YoutubeVideoPlayer',
		title: 'Youtube',
		description: 'Youtube video player',
		groups: ['commons'],
		icon: 'wb-common-youtube',
		// help
		help: '', 
		helpId: 'youTube',
		// page
		templateUrl: 'views/am-wb-common-widgets/youtube-audio-player.html',
		setting: ['common-video-player', 'SEO'],
		controllerAs: 'ctrl',
		/*
		 * @ngInject
		 */
		controller: function ($wbLibs) {
			var keys = ['url', 'description', 'label', 'keywords'];
			var defaultUrl = 'https://www.youtube.com/watch?v=qb7pOyNnXUg&list=PLcbg8k6WnJMW4x_V-HPqOJ_xSdO_Vz7C5';
			/**
			 * Reduce size of the widget and return a valid height for player
			 * 
			 */
			this.getHeight = function (value) {
				if (!value) {
					return null;
				}

				var unit = value.replace(/^[-\d\.]+/, '');
				var newVal = parseFloat(value, 10) - 10;
				return newVal + unit;
			};
			/**
			 * Initialize the widget
			 * 
			 * @memberof AmWbCommonVideoCtrl
			 */
			this.initWidget = function () {
				var ctrl = this;
				function doTask($event) {
					if (keys.indexOf($event.key) > -1) {
						var key = $event.key;
						ctrl[key] = ctrl.getProperty(key) || ctrl.getModelProperty(key);
					}
				}
				/**
				 * Loads all parameters from model
				 * 
				 * @memberof AmWbCommonVideoCtrl
				 */
				this.fillMedia = function () {
					for (var i = 0; i < keys.length; i++) {
						var key = keys[i];
						this[key] = this.getModelProperty(key);
					}
					this.url = this.getModelProperty('url') || defaultUrl;
				};
				this.on('modelUpdated', function ($event) {
					doTask($event);
				});
				this.on('runtimeModelUpdated', function ($event) {
					doTask($event);
				});
				this.on('modelChanged', function () {
					this.fillMedia();
				});
				//handling 'youtube.player.ready' event and wrap it into 'success' event
				this.getScope().$on('youtube.player.ready', function (event) {
					event.source = ctrl;
					event.key = 'success';
					ctrl.fire('success', event);
				});
				//handling 'youtube.player.error' event and wrap it into 'failure' event
				this.getScope().$on('youtube.player.error', function (event) {
					event.source = ctrl;
					event.key = 'failure';
					ctrl.fire('error', event);
				});
				this.fillMedia();
			};
			$wbLibs.load('https://www.youtube.com/iframe_api');
		}
	});
	$widget.newWidget({
		// widget
		type: 'AparatVideoPlayer',
		title: 'Aparat',
		description: 'Aparat video player',
		groups: ['commons'],
		icon: 'wb-common-aparat',
		// help
		help: '',
		helpId: 'aparat',
		// page
		templateUrl: 'views/am-wb-common-widgets/aparat-audio-player.html',
		setting: ['common-video-player', 'SEO'],
		controllerAs: 'ctrl',
		/*
		 * @ngInject
		 */
		controller: function ($sce) {
			var keys = ['fit', 'url', 'description', 'label', 'keywords'];
			var defaultUrl = 'https://www.aparat.com/v/lPRVs';

			function getAparatFrameLink(murl) {
				var hcode = murl.substring(murl.lastIndexOf('/') + 1, murl.length + 1);
				var url = murl;
				if (hcode !== 'live') {
					url = 'https://www.aparat.com/video/video/embed/videohash/' + hcode + '/vt/frame';
				}
				return $sce.trustAsResourceUrl(url);
			}


			/**
			 * Loads all parameters from model
			 * 
			 * @memberof AmWbCommonVideoCtrl
			 */
			this.fillMedia = function () {
				for (var i = 0; i < keys.length; i++) {
					var key = keys[i];
					this[key] = this.getModelProperty(key);
				}
				this.frameUrl = getAparatFrameLink(this.url || defaultUrl);
			};

			/**
			 * Initialize the widget
			 * 
			 * @memberof AmWbCommonVideoCtrl
			 */
			this.initWidget = function () {
				var ctrl = this;

				// pass event to setting panel if video loaded successfully
				this.onLoad = function () {
					var event = {};
					event.source = ctrl;
					event.key = 'success';
					ctrl.fire('success', event);
				};

				// pass event to setting panel if video doesn't load successfully
				this.onError = function () {
					var event = {};
					event.source = ctrl;
					event.key = 'failure';
					ctrl.fire('error', event);
				};

				function doTask($event) {
					if (keys.indexOf($event.key) > -1) {
						var key = $event.key;
						ctrl[key] = ctrl.getProperty(key) || ctrl.getModelProperty(key);
					}
					if ($event.key === 'url') {
						ctrl.frameUrl = getAparatFrameLink(ctrl.url || defaultUrl);
					}
				}

				this.on('modelUpdated', function ($event) {
					doTask($event);
				});
				this.on('runtimeModelUpdated', function ($event) {
					doTask($event);
				});
				this.on('modelChanged', function () {
					ctrl.fillMedia();
				});
				this.fillMedia();
			};
		}
	});
	
	$widget.newWidget({
		type: 'NavBar',
		title: 'NavBar',
		description: 'Centerelized different materials about one category.',
		groups: ['commons', 'group'],
		icon: 'subject',
		// help
		help: '',
		helpId: 'nav-bar',
		// functional (page)
		templateUrl: 'views/am-wb-common-widgets/nav-bar.html',
		setting: ['nav-bar'],
		model: {
			tabs: [{
				type: 'Group',
				label: 'Tab1',
				contents:[{
					text: '<h2>This is a Text widget</h2><p>You could delete it and design the tab with your favorite widgets.</p>',
					type: 'HtmlText',
					name: 'Text',
					event: {},
					style: {
						layout: {},
						size: {},
						background: {},
						border: {},
						align: {}
					},
					version: 'wb1'
				}]
			},
			{
				type: 'Group',
				label: 'Tab2',
				contents:[{
					text: '<h2>This is a Text widget</h2><p>You could delete it and design the tab with your favorite widgets.</p>',
					type: 'HtmlText',
					name: 'Text',
					event: {},
					style: {
						layout: {},
						size: {},
						background: {},
						border: {},
						align: {}
					},
					version: 'wb1'
				}]
			}]
		}
	});
	/**
	 * @ngdoc Widgets
	 * @name Shape
	 * @description display an SVG shape
	 * 
	 * @deprecated replaced with core/svg
	 */
	$widget.newWidget({
		type: 'Shape',
		title: 'Shape',
		description: 'SVG Shape.',
		groups: ['commons'],
		icon: 'category',
		// help
		help: 'am-wb-widget-shape',
		helpId: 'am-wb-widget-shape',
		// functional (page)
		template: '',
		setting: ['shape'],
		controllerAs: 'ctrl',
		/*
		 * @ngInject
		 */
		controller: function () {
			// local variables
			this._shape;
			this._svg;
			this.mainKeys = [
				'content',
				'style.size.width',
				'style.size.height',
				'style.fill',
				'style.stroke',
				'style.strokeWidth'
				];
			this.calculateProperty = function (key, defaultValue) {
				return this.getProperty(key) || this.getModelProperty(key) || defaultValue;
			};
			this.calculateSvg = function () {
				var content = this.calculateProperty('content', '<svg width="100" height="100"><circle cx="50" cy="50" r="40" stroke="green" stroke-width="4" fill="yellow" /></svg>');
				if (!this._svg || content !== this._shape) {
					// create element
					this._shape = content;
					var shapeEle = angular.element(this._shape);
					// insert to view
					var $element = this.getElement();
					$element.empty();
					$element.append(shapeEle);
					this._svg = d3.select(shapeEle[0]);
				}
				return this._svg;
			}

			this.updateShape = function () {
				var svg = this.calculateSvg();
				var $element = this.getElement();
				// size
				svg
				.style('width', $element.width())
				.style('height', $element.height());
				// color
				svg.selectAll('path')
				.style('fill', this.calculateProperty('style.fill', 'black'))
				.style('stroke', this.calculateProperty('style.stroke', 'black'))
				.style('stroke-width', this.calculateProperty('style.strokeWidth', '1px'));
			};
			// init widget
			this.initWidget = function () {
				var ctrl = this;
				this.on('modelUpdated', function ($event) {
					var key = $event.key;
					if (ctrl.mainKeys.includes(key)) {
						ctrl.updateShape();
					}
				});
				this.on('runtimeModelUpdated', function ($event) {
					var key = $event.key;
					if (ctrl.mainKeys.includes(key)) {
						ctrl.updateShape();
					}
				});
				this.on('resize', function () {
					// TODO: maso, 2019: just update the size
					ctrl.updateShape();
				});
				this.updateShape();
			};
		}
	});
	/**
	 * @ngdoc Widgets
	 * @name Button
	 * @description A Button to do special task
	 * 
	 */
	$widget.newWidget({
		type: 'Button',
		title: 'Button',
		description: 'A Button to do special task',
		groups: ['commons'],
		icon: 'button',
		// help
		help: '',
		helpId: '',
		// functional (page)
		templateUrl: 'views/am-wb-common-widgets/button.html'
	});
	/**
	 * @ngdoc Widgets
	 * @name Import
	 * @description Import external resource into the current document
	 * 
	 */
	$widget.newWidget({
		type: 'Import',
		title: 'Import',
		description: 'Import a part of other content',
		groups: ['commons'],
		icon: 'import_export',
		setting: ['import'],
		// help
		help: '',
		helpId: '',
		// functional (page)
		templateUrl: 'views/am-wb-common-widgets/import.html',
		/*
		 * @ngInject
		 */
		controller: function ($wbUtil, $http) {

			/*
			 * NOTE: this is a utility function and can move to UTILSE
			 */
			this.findWidgetModelById = function (wbModel, id) {
				if (wbModel.id === id) {
					return wbModel;
				}
				if (wbModel.type === 'Group') {
					for (var i = 0; i < wbModel.contents.length; i++) {
						var model = this.findWidgetModelById(wbModel.contents[i], id);
						if (model) {
							return model;
						}
					}
				}
				return null;
			};
			/*
			 * NOTE: this is a utility function and can move to UTILSE
			 */
			this.downloadWidgetModel = function (url, id) {
				// TODO: maso, 2019: CHECK if URL is @configuration
				var ctrl = this;
				return $http.get(url)
				.then(function (res) {
					var totalModel = $wbUtil.clean(res.data);
					if (!id) {
						return totalModel;
					}
					// XXX: maso, 2019: find part of the models
					return ctrl.findWidgetModelById(totalModel, id);
				});
			};
			/*
			 * Load link data
			 */
			this.loadLink = function () {
				this.loading = true;
				// check if the url
				var path = this.getModelProperty('url');
				if (!path) {
					delete this.model;
					return;
				}

				// check parts
				var parts = path.split("#");
				if (!parts.length) {
					delete this.model;
					return;
				}
				//
				this.url = parts[0];
				this.id = parts.length > 1 ? parts[1] : undefined;
				var ctrl = this;
				this.downloadWidgetModel(this.url, this.id)
				.then(function (model) {
					ctrl.model = model;
					ctrl.loading = false;
				});
			};
			this.selectionEventHandler = function ($event) {
				var widget = $event.widgets[0];
				this.importedWidget = widget.getRoot();
			};
			this.getImportedWidget = function () {
				return this.importedWidget;
			};
			/*
			 * Init widget
			 */
			this.initWidget = function () {
				var ctrl = this;
				this.model = {
						type: 'Group'
				};
				this.on('modelUpdated', function ($event) {
					if ($event.key === 'url') {
						ctrl.loadLink();
					}
				});
				this.loadLink();
			};
		}
	});
});

angular.module('am-wb-common').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/am-wb-common-dialogs/feature.html',
    "<md-dialog aria-label=\"Feature item config\" ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <h2 translate>Link settings</h2> <span flex></span> <md-button aria-label=Done class=md-icon-button ng-click=answer(model)> <wb-icon aria-label=\"Save change\">done</wb-icon> </md-button> <md-button aria-label=Close class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout-padding> <md-tabs md-dynamic-height md-border-bottom> <md-tab label=View> <md-content layout=column>  <wb-ui-setting-image title=Image ng-model=model.image> </wb-ui-setting-image> <md-input-container> <label translate>Title</label> <input ng-model=model.title> </md-input-container> <md-input-container> <label translate>Text</label> <input ng-model=model.text> </md-input-container> </md-content> </md-tab> <md-tab label=Action> <md-content layout=column> <md-input-container> <label translate>Select type</label> <md-select ng-model=model.action.type> <md-option value=link> <span translate>Link</span> </md-option> <md-option value=internal-link> <span translate>Internal link</span> </md-option> </md-select> </md-input-container> <md-input-container> <label translate>{{model.action.type}}</label> <input ng-model=model.action.link> </md-input-container> <md-input-container> <label translate>Label</label> <input ng-model=model.action.label> </md-input-container> </md-content> </md-tab> </md-tabs> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/am-wb-common-settings/action.html',
    " <md-list class=wb-setting-panel> <md-input-container class=\"md-icon-float md-block\"> <label translate>Label</label> <input ng-model=ctrl.action.label ng-model-options={debounce:500} ng-change=ctrl.updateAction(ctrl.action)> </md-input-container> <wb-ui-setting-dropdown title=\"Action type\" icon=border_left items=types ng-model=.action.type ng-model-options={debounce:500} ng-change=ctrl.updateAction(ctrl.action)> </wb-ui-setting-dropdown>  <md-input-container ng-show=\"ctrl.action.type=='link'\" class=\"md-icon-float md-block\"> <label>Link</label> <input ng-model=ctrl.action.link ng-model-options={debounce:500} ng-change=ctrl.updateAction(ctrl.action)> </md-input-container> </md-list>"
  );


  $templateCache.put('views/am-wb-common-settings/audio-player.html',
    "<md-list class=wb-setting-panel>  <wb-ui-setting-audio title=Source ng-model=ctrl.content.url ng-model-options={debounce:500} ng-change=ctrl.contentChanged()> </wb-ui-setting-audio> <md-input-container class=\"md-icon-float md-block\"> <label trnslate>Type</label> <input ng-model=ctrl.content.mimeType ng-model-options={debounce:500} ng-change=ctrl.contentChanged() aria-label=\"MIME Type\"> </md-input-container> <wb-ui-setting-on-off-switch title=Autoplay? icon=play_arrow ng-model=ctrl.setting.autoplay ng-model-options={debounce:500} ng-change=ctrl.settingChanged()> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=Controls? icon=swap_horiz ng-model=ctrl.setting.controls ng-model-options={debounce:500} ng-change=ctrl.settingChanged()> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=Loop? icon=replay ng-model=ctrl.setting.loop ng-model-options={debounce:500} ng-change=ctrl.settingChanged()> </wb-ui-setting-on-off-switch> <wb-ui-setting-dropdown title=Preload icon=cached items=\"[{'title':'auto','value':'auto'},\n" +
    "\t\t\t\t{'title':'metadata','value':'metadata'},\n" +
    "\t\t\t\t{'title':'none','value':'none'}]\" ng-model=ctrl.setting.preload ng-model-options={debounce:500} ng-change=ctrl.settingChanged()> </wb-ui-setting-dropdown> </md-list>"
  );


  $templateCache.put('views/am-wb-common-settings/features.html',
    "<div class=md-toolbar-tools> <span flex></span> <md-button ng-click=ctrl.addFeature() class=md-icon-button aria-label=\"Add featrue\"> <wb-icon>add</wb-icon> </md-button> </div> <md-list> <md-list-item ng-repeat=\"feature in ctrl.features track by $index\" ng-click=\"ctrl.editFeature(feature, $index)\" class=noright> <img ng-src={{feature.image}} width=32px height=32px class=\"md-avatar-icon\"> <p>{{feature.title}}</p> <wb-icon ng-show=\"$index<ctrl.features.length-1\" ng-click=ctrl.moveDownFeature($index) class=\"md-secondary md-hue-3\" aria-label=\"Move down\">wb-common-movedown</wb-icon> <wb-icon ng-show=\"$index>0\" ng-click=ctrl.moveUpFeature($index) class=\"md-secondary md-hue-3\" aria-label=\"Move up\">wb-common-moveup</wb-icon> <wb-icon class=md-secondary ng-click=ctrl.removeFeature($index) aria-label=Delete>delete</wb-icon> </md-list-item> </md-list>"
  );


  $templateCache.put('views/am-wb-common-settings/import.html',
    "<div layout-padding layout=column> <wb-ui-setting-link title=URL ng-model=ctrl.url ng-change=ctrl.urlChanged() ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\"> </wb-ui-setting-link> <a ng-if=ctrl.canonicalLink href={{ctrl.canonicalLink}} target=_blank> <span translate=\"\">Navigate to the page containing imported widget</span> </a> </div>"
  );


  $templateCache.put('views/am-wb-common-settings/navigation-bar.html',
    "<div layout=column> <div layout=row flex> <md-button ng-click=ctrl.addTab()> <span translate=\"\">New tab</span> </md-button> </div> <md-subheader class=md-hue-3> <span translate>List of tabs</span> </md-subheader> <md-list flex> <md-list-item ng-repeat=\"tab in wbModel.tabs track by $index\"> <img ng-if=tab.icon ng-src={{tab.icon}} class=\"md-avatar\"> <am-wb-inline ng-model=tab.label am-wb-inline-type=text am-wb-inline-label=Title am-wb-inline-enable=true> <span>{{tab.label}}</span> </am-wb-inline> <wb-icon ng-click=ctrl.deleteTab($index) class=md-secondary aria-lable=\"Delete tab\">delete</wb-icon> </md-list-item> </md-list> </div>"
  );


  $templateCache.put('views/am-wb-common-settings/shape.html',
    "<wb-ui-setting-color title=Fill wb-ui-setting-clear-button=true wb-ui-setting-preview=true ng-model=ctrl.fill ng-change=\"ctrl.styleChanged('fill', ctrl.fill)\"> </wb-ui-setting-color> <wb-ui-setting-color title=Stroke wb-ui-setting-clear-button=true wb-ui-setting-preview=true ng-model=ctrl.stroke ng-change=\"ctrl.styleChanged('stroke', ctrl.stroke)\"> </wb-ui-setting-color> <wb-ui-setting-length title=\"Stroke width\" icon=corner_bottom_right ng-model=ctrl.strokeWidth ng-change=\"ctrl.styleChanged('strokeWidth', ctrl.strokeWidth)\" extra-values=[]> </wb-ui-setting-length> <md-input-container> <label translate>Content</label> <input ng-model-options=\"{updateOn : 'change blur'}\" ng-model=ctrl.content ng-change=ctrl.contentChanged(ctrl.content)> </md-input-container>"
  );


  $templateCache.put('views/am-wb-common-settings/video-player.html',
    " <fieldset layout=column> <legend translate>Media</legend> <am-wb-setting-youtube ng-if=\"ctrl.type === 'YoutubeVideoPlayer'\" title=\"YouTube source\" lable=\"Enter YouTube URL\" ng-model=ctrl.url ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=ctrl.updateUrl()> </am-wb-setting-youtube> <am-wb-setting-aparat ng-if=\"ctrl.type === 'AparatVideoPlayer'\" title=\"Aparat source\" lable=\"Enter aparat URL\" ng-model=ctrl.url ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=ctrl.updateUrl()> </am-wb-setting-aparat> <wb-ui-setting-video ng-if=\"ctrl.type === 'CommonVideoPlayer'\" title=\"Video source\" lable=\"Enter video URL\" ng-model=ctrl.url ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=ctrl.updateUrl()> </wb-ui-setting-video> <md-input-container ng-show=\"ctrl.type === 'CommonVideoPlayer'\" class=\"md-icon-float md-block\"> <label>Type</label> <input ng-model=ctrl.mimeType ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=ctrl.updateMimeType() aria-label=\"MIME Type\"> </md-input-container> </fieldset> <fieldset ng-if=\"ctrl.type === 'CommonVideoPlayer'\" layout=column> <legend translate>Preview</legend> <wb-ui-setting-image title=Poster ng-model=ctrl.poster ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=ctrl.updatePoster()> </wb-ui-setting-image> <wb-ui-setting-dropdown title=Fit icon=cached items=\"[{'title':'fill','value':'fill'},\n" +
    "        {'title':'contain','value':'contain'},\n" +
    "        {'title':'cover','value':'cover'},\n" +
    "        {'title':'scale-down','value':'scale-down'},\n" +
    "        {'title':'none','value':'none'}]\" ng-model=ctrl.fit ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=ctrl.updateFit()> </wb-ui-setting-dropdown> <wb-ui-setting-dropdown title=Preload icon=cached items=\"[{'title':'auto','value':'auto'},\n" +
    "        {'title':'metadata','value':'metadata'},\n" +
    "        {'title':'none','value':'none'}]\" ng-model=ctrl.preload ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=ctrl.updatePreload()> </wb-ui-setting-dropdown> </fieldset> <fieldset layout=column> <legend translate>Controllers</legend> <wb-ui-setting-on-off-switch title=Autoplay icon=play_arrow ng-model=ctrl.autoplay ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=ctrl.updateAutoplay()> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=Controls icon=swap_horiz ng-model=ctrl.controls ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=ctrl.updateControls()> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=Loop icon=replay ng-model=ctrl.loop ng-model-options=\"{debounce: {'default': 500, 'blur': 0, '*': 1000}, updateOn: 'default blur click'}\" ng-change=ctrl.updateLoop()> </wb-ui-setting-on-off-switch> </fieldset>"
  );


  $templateCache.put('views/am-wb-common-widgets/action-call.html',
    "<div layout=column layout-align=\"center center\"> <h1 class=md-display-3 ng-style=\"{'color': wbModel.style.color, 'text-align':'center'}\" ng-class=\"{'dd-rtl':wbModel.style.rtl}\"> {{ wbModel.label }} </h1> <span></span> <h4 class=md-display-1 ng-style=\"{'color': wbModel.style.color, 'text-align':'center'}\" ng-class=\"{'dd-rtl':wbModel.style.rtl}\" hide show-gt-xs>{{wbModel.description}}</h4> <div wb-layout=wbModel.style> <md-button ng-repeat=\"feature in wbModel.features\" ng-style=\"{'color': wbModel.style.color, 'font-size': '20px', 'padding':'5px 20px'}\" class=\"md-button md-raised md-primary\" ng-href={{wbModel.action.link}} ng-click=featuresCtrl.runAction(feature)> {{ feature.title }} </md-button> </div> </div>"
  );


  $templateCache.put('views/am-wb-common-widgets/aparat-audio-player.html',
    "<iframe id=media frameborder=0 scrolling=no seamless src={{ctrl.frameUrl}} ng-class=\"{ 'object-fill': ctrl.fit === 'fill', 'object-contain': ctrl.fit === 'contain', 'object-cover': ctrl.fit === 'cover', 'object-none': ctrl.fit === 'none', 'object-scale-down': ctrl.fit === 'scale-down'}\" allowfullscreen webkitallowfullscreen=true mozallowfullscreen=true wb-on-load=ctrl.onLoad() wb-on-error=ctrl.onError()> </iframe>"
  );


  $templateCache.put('views/am-wb-common-widgets/audio-player.html',
    "<audio ng-attr-autoplay=\"{{wbModel.setting.autoplay | trueOrUndefined}}\" ng-attr-controls=\"{{wbModel.setting.controls | trueOrUndefined}}\" ng-attr-loop=\"{{wbModel.setting.loop | trueOrUndefined}}\" poster=images/wallpaper.jpg preload={{wbModel.setting.preload}}> <source src={{wbModel.content.url}} type=\"{{wbModel.content.mimeType}}\"> <span translate>Your browser does not support the audio tag. It seems your browser is too old!</span> </audio>"
  );


  $templateCache.put('views/am-wb-common-widgets/button.html',
    "<md-button class=\"md-primary md-raised\">{{wbModel.label}}</md-button>"
  );


  $templateCache.put('views/am-wb-common-widgets/feature-links.html',
    "<div wb-layout=wbModel.style>  <md-button ng-repeat=\"feature in wbModel.features track by $index\" class=md-primary ng-click=\"featuresCtrl.runAction(feature, null, $event)\" ng-href={{feature.action.link}}> <wb-icon ng-if=feature.action.icon>{{feature.icon}}</wb-icon> {{feature.title}} </md-button> </div>"
  );


  $templateCache.put('views/am-wb-common-widgets/feature-list.html',
    "<div>  <div ng-hide=ctrl.isSelected() ng-bind-html=\"wbModel.text | wbunsafe\"> </div> <div ui-tinymce=\"{\n" +
    "\t    selector : 'div.tinymce',\n" +
    "\t    theme : 'inlite',\n" +
    "\t    plugins : 'directionality contextmenu table link paste image imagetools hr textpattern autolink textcolor colorpicker ',\n" +
    "\t    insert_toolbar : 'quickimage quicktable',\n" +
    "\t    selection_toolbar : 'bold italic | quicklink h1 h2 h3 blockquote | ltr rtl | forecolor',\n" +
    "\t    insert_button_items: 'image link | inserttable | hr',\n" +
    "\t    inline : true,\n" +
    "\t    paste_data_images : true,\n" +
    "\t    branding: false,\n" +
    "\t    imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions'\n" +
    "\t    }\" ng-model=wbModel.text ng-show=ctrl.isSelected() flex> </div> <div ng-class=\"{'dd-rtl':wbModel.style.rtl}\" layout=column layout-align=\"start center\" layout-gt-sm=\"{{wbModel.layout|| 'row'}}\" layout-align-gt-sm=\"center start\" ng-style=\"{'width': '100 % '}\"> <div style=width:100% layout=column layout-align=\"start center\" layout-padding ng-repeat=\"s in wbModel.features\" ng-class=\"{'feature-list-link': s.action.type}\" ng-click=\"featuresCtrl.runAction(s, null, $event)\"> <img ng-src={{s.image}} width=\"{{wbModel.style.iconWidth || '200px'}}\" height=\"{{wbModel.style.iconHeight || '200px'}}\"> <h3>{{s.title}}</h3> <div ng-hide=ctrl.isSelected() ng-bind-html=\"s.text | wbunsafe\"> </div> <div ui-tinymce=\"{\n" +
    "\t            selector : 'div.tinymce',\n" +
    "\t            theme : 'inlite',\n" +
    "\t            plugins : 'directionality contextmenu table link paste image imagetools hr textpattern autolink textcolor colorpicker ',\n" +
    "\t            insert_toolbar : 'quickimage quicktable',\n" +
    "\t            selection_toolbar : 'bold italic | quicklink h1 h2 h3 blockquote | ltr rtl | forecolor',\n" +
    "\t            insert_button_items: 'image link | inserttable | hr',\n" +
    "\t            inline : true,\n" +
    "\t            paste_data_images : true,\n" +
    "\t            branding: false,\n" +
    "\t            imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions'\n" +
    "\t            }\" ng-model=s.text ng-show=ctrl.isSelected() flex> </div> </div> <md-button ng-if=wbEditable aria-label=\"Add feature\" ng-click=featuresCtrl.addFeature()> <wb-icon>add</wb-icon> </md-button> </div> </div>"
  );


  $templateCache.put('views/am-wb-common-widgets/feature-mozaic.html',
    "<div layout=row layout-wrap layout-padding>  <div layout=column layout-align=\"center start\" flex=100 flex-gt-md=50 ng-repeat=\"tile in wbModel.features\"> <h3>{{tile.title}}</h3> <p class=md-body-1>{{tile.text}}</p> <md-button ng-if=tile.action.type class=md-primary ng-click=\"featuresCtrl.runAction(tile, null, $event)\" ng-href={{tile.action.link}}> <md-icon ng-if=tile.action.icon>{{tile.icon}}</md-icon> <span>More</span> </md-button> </div> <md-button class=\"md-raised md-primary md-icon-button\" ng-if=wbEditable aria-label=\"Add feature\" ng-click=featuresCtrl.addFeature()> <wb-icon>add</wb-icon> </md-button> </div>"
  );


  $templateCache.put('views/am-wb-common-widgets/feature-toolbar.html',
    "<md-toolbar wb-border=wbModel.style layout=row layout-align=\"center center\"> <img ng-if=wbModel.cover height=42px ng-src={{wbModel.cover}} ng-class=\"md-icon-button\"> <md-truncate ng-if=wbModel.label layout-padding> {{ wbModel.label }}</md-truncate> <am-wb-feature-button hide show-gt-md ng-repeat=\"feature in wbModel.features | limitTo: 8\" ng-model=feature wb-icon-button=feature.image> <img ng-if=feature.image height=32px ng-src=\"{{ feature.image }}\"> <span ng-if=!feature.image>{{ feature.title }}</span> <md-tooltip ng-if=feature.text md-delay=1500>{{ feature.text }}</md-tooltip> </am-wb-feature-button> <span flex></span> <md-menu ng-show=wbModel.features.length> <md-button aria-label=\"Open demo menu\" class=md-icon-button ng-click=$mdMenu.open($event)> <wb-icon>more_vert</wb-icon> </md-button> <md-menu-content> <md-menu-item ng-repeat=\"feature in wbModel.features\"> <md-button show-gt-md aria-label=Menu ng-click=\"featuresCtrl.runAction(feature, null, $event)\"> <img height=24px ng-src=\"{{ feature.image }}\"> {{ feature.title }} </md-button> </md-menu-item> </md-menu-content> </md-menu> </md-toolbar>"
  );


  $templateCache.put('views/am-wb-common-widgets/import.html',
    "<div id=am-wb-common-media-container class=wb-widget-fill>  <wb-group wb-editable=false ng-model=ctrl.model wb-on-model-select=ctrl.selectionEventHandler($event) id=media> </wb-group> <div id=cover ng-if=ctrl.isEditable()></div> </div>"
  );


  $templateCache.put('views/am-wb-common-widgets/nav-bar.html',
    "<div class=wb-widget-fill layout=column layout-padding> <md-tabs flex md-selected=selectedIndex md-border-bottom md-autoselect md-swipe-content> <md-tab ng-repeat=\"tab in wbModel.tabs\"> <md-tab-label> {{tab.label}} </md-tab-label> <md-tab-body> <md-content style=\"background-color: inherit\"> <wb-group wb-on-model-select=loadSettings($event) wb-editable=editable ng-model=wbModel.tabs[selectedIndex] flex> </wb-group> </md-content> </md-tab-body> </md-tab> </md-tabs> </div>"
  );


  $templateCache.put('views/am-wb-common-widgets/video-player.html',
    "<video id=media ng-attr-autoplay=\"{{ctrl.autoplay | trueOrUndefined}}\" ng-attr-controls=\"{{ctrl.controls | trueOrUndefined}}\" ng-attr-loop=\"{{ctrl.loop | trueOrUndefined}}\" poster={{ctrl.poster}} preload={{ctrl.preload}} ng-class=\"{ 'object-fill': ctrl.fit === 'fill', 'object-contain': ctrl.fit === 'contain', 'object-cover': ctrl.fit === 'cover', 'object-none': ctrl.fit === 'none', 'object-scale-down': ctrl.fit === 'scale-down'}\"> <source src={{ctrl.url}} type=\"{{ctrl.mimeType}}\"> <span>Your browser does not support the video tag. It seems your browser is too old!</span> </video>"
  );


  $templateCache.put('views/am-wb-common-widgets/youtube-audio-player.html',
    "<div itemscope itemtype=http://schema.org/MovieClip style=\"width: 100%; height: 100%\">  <span itemprop=description ng-show=false>{{ctrl.description}}</span> <span itemprop=alternativeHeadline ng-show=false>{{ctrl.label}}</span> <span itemprop=discussionUrl ng-show=false>{{ctrl.url}}</span> <span itemprop=keywords ng-show=false>{{ctrl.keywords}}</span> <youtube-video ng-if=\"ctrl.url && ctrl.isEditable()\" style=pointer-events:none player-width=\"'100%'\" player-height=\"'100%'\" video-url=ctrl.url> </youtube-video> <youtube-video ng-if=\"ctrl.url && !ctrl.isEditable()\" player-width=\"'100%'\" player-height=\"'100%'\" video-url=ctrl.url> </youtube-video> </div>"
  );


  $templateCache.put('views/directives/am-wb-feature-button.html',
    "<div> <md-button ng-if=!ctrl.type aria-label={{feature.title}} ng-class=\"{'md-icon-button': wbIconButton}\"> <ng-transclude flex></ng-transclude> </md-button>  <md-button ng-if=\"ctrl.type === 'action' && editable\" aria-label={{feature.title}} ng-class=\"{'md-icon-button': wbIconButton}\"> <ng-transclude flex></ng-transclude> </md-button> <md-button ng-if=\"ctrl.type === 'action' && !editable\" aria-label={{feature.title}} ng-click=runAction() ng-class=\"{'md-icon-button': wbIconButton}\"> <ng-transclude flex></ng-transclude> </md-button>  <md-button ng-if=\"ctrl.type === 'link' && editable\" aria-label={{feature.title}} ng-class=\"{'md-icon-button': wbIconButton}\"> <ng-transclude flex></ng-transclude> </md-button> <md-button ng-if=\"ctrl.type === 'link' && !editable\" aria-label={{feature.title}} ng-href={{ctrl.link}} ng-class=\"{'md-icon-button': wbIconButton}\"> <ng-transclude flex></ng-transclude> </md-button>  <md-button ng-if=\"ctrl.type === 'internal-link' && editable\" aria-label={{feature.title}} ng-class=\"{'md-icon-button': wbIconButton}\"> <ng-transclude flex></ng-transclude> </md-button> <md-button ng-if=\"ctrl.type === 'internal-link' && !editable\" aria-label={{feature.title}} ng-click=runAction() ng-class=\"{'md-icon-button': wbIconButton}\"> <ng-transclude flex></ng-transclude> </md-button> </div>"
  );


  $templateCache.put('views/directives/am-wb-setting-aparat.html',
    "<div> <md-button class=md-icon-button> <wb-icon>wb-common-aparat</wb-icon> </md-button> <md-input-container> <input ng-model=value ng-change=valueChanged(value)> </md-input-container> </div>"
  );


  $templateCache.put('views/directives/am-wb-setting-youtube.html',
    "<div> <md-button class=md-icon-button aria-label=\"Youtube clip\"> <wb-icon>wb-common-youtube</wb-icon> </md-button> <md-input-container> <input ng-model=value ng-change=valueChanged(value)> </md-input-container> </div>"
  );

}]);
