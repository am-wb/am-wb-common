# Common WB widgets

[![pipeline status](https://gitlab.com/am-wb/am-wb-common/badges/master/pipeline.svg)](https://gitlab.com/am-wb/am-wb-common/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/898f3f5fd7ea4c58a7ce310a10667fa5)](https://www.codacy.com/app/am-wb/am-wb-common?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=am-wb/am-wb-common&amp;utm_campaign=Badge_Grade)

Common widgets for WebBurger.

See more details in [technical document](https://am-wb.gitlab.io/am-wb-common/doc/index.html).


# Data types

- action
- feature item

## action

Action is an executable part of the system. You can bind an action into any part of 
the system.

Supported types:

- link

Here is an example of action:

	{
		"type": "link",
		"link": "http://google.com"
	}
	
## Feature item

Here is a feature item data model:


	{
		"title": "Title",
		"text": "Description",
		"icon": "icon url",
		"action": {
			"type": "link",
			"link": "http://google.com"
		}
	}

# Widgets

- image
