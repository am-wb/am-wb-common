'use strict';

angular.module('am-wb-common')
/**
 * @ngdoc function
 * @name AmWbCommonDialogmodelCtrl
 * @description # Dialog controller
 * 
 * This is common dialog controller.
 */
.controller('AmWbCommonDialogmodelCtrl', function($scope, $mdDialog, style, model) {
	$scope.model = model;
	$scope.style = style;
	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
		$mdDialog.cancel();
	};
	$scope.answer = function(a) {
		$mdDialog.hide(a);
	};
});
