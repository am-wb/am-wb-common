/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('am-wb-common')
/**
 * @ngdoc Controllers
 * @name AmWbCommonFeaturesCtrl
 * @description control some general properties of different widgets.
 * 
 * The controller is used with widgets
 */
.controller('AmWbCommonFeaturesCtrl', function($scope, $wbUi, $window, $resource,
        $anchorScroll, $location) {
    var ngModel = $scope.wbModel;

    function addFeature() {//
        // first version of model
        var feature =  {
                'icon' : 'assignment_turned_in',
                'title' : 'Item title',
                'text' : 'Item description.',
                action : {
                    type : 'link',
                    link : ''
                }
        };
        /*
         * 
         * this function uses an internal function named
         * $mdDialog.show. $mdDialog.show shows a dialog box
         * using an input data and waits until the dialog box be
         * closed. Then it returns a new version of data. So, if
         * we want to do some works over output of dialog box,
         * we need to do this works inside
         * '.then(function(newData))' which comes just next to
         * the calling $mdDialog.show function. if we don't want
         * to do work over output (want the dialog service just
         * to change the input data and replace the old version)
         * we don't need to .then part). the function
         * editFeature() comes later in this controller works
         * this way.
         */
        $wbUi.openDialog(
                {
                    controller : 'AmWbCommonDialogmodelCtrl',
                    templateUrl : 'views/am-wb-common-dialogs/feature.html',
                    parent : angular
                    .element(document.body),
                    clickOutsideToClose : true,
                    locals : {
                        model : feature,
                        style : {
                            title : 'service'
                        }
                    }
                }).then(function(newFeature) {
                    // Example of how $mdDialog.show return a
                    // new version of its input:
                    // https://stackoverflow.com/questions/41650984/angularjs-mddialog-how-to-send-back-data-to-my-first-controller
                    ngModel.features.push({
                        'icon' : newFeature.icon,
                        'title' : newFeature.title,
                        'text' : newFeature.text,
                        action : {
                            type : newFeature.action.type,
                            link : newFeature.action.link
                        }
                    });
                });
    }

    function removeFeature(feature) {
        var index = ngModel.features.indexOf(feature);
        if (index > -1) {
            ngModel.features.splice(index, 1);
        }
    }

    function removeAllFeatures() {
        ngModel.features = [];
    }

    function editFeature(feature) {
        $wbUi.openDialog({
            controller : 'AmWbCommonDialogmodelCtrl',
            templateUrl : 'views/am-wb-common-dialogs/feature.html',
            parent : angular.element(document.body),
            clickOutsideToClose : true,
            locals : {
                model : feature,
                style : {
                    title : 'service'
                }
            }
        });
    }

    function goToAnchor(anchor) {
        if ($location.hash() !== anchor) {
            // set the $location.hash to `anchor` and
            // $anchorScroll will automatically scroll to it
            $location.hash(anchor);
        } else {
            // call $anchorScroll() explicitly,
            // since $location.hash hasn't changed
            $anchorScroll();
        }
    }

    function moveArrayItem(array, old_index, new_index) {
        if (new_index >= array.length) {
            var k = new_index - array.length;
            while ((k--) + 1) {
                array.push(undefined);
            }
        }
        array.splice(new_index, 0,
                array.splice(old_index, 1)[0]);
    }

    function moveUpFeature(feature) {
        var index = ngModel.features.indexOf(feature);
        if (index === 0) {
            return;
        }
        moveArrayItem(ngModel.features, index, index - 1);
    }

    function moveDownFeature(feature) {
        var index = ngModel.features.indexOf(feature);
        if (index === ngModel.features.length - 1) {
            return;
        }
        moveArrayItem(ngModel.features, index, index + 1);
    }

    /*
     * run feature action
     */
    function runAction(feature, params, event) {
        // TODO: support page parameters
        var link = feature.action.link;
        var type = feature.action.type;
        if (!$scope.ctrl.isEditable()) {
            // Ex. 'https://www.google.com'
            if (link.toLowerCase().startsWith('http')) {
                $window.open(link);
            }
            if (type === 'link') {
                if (params) {
                    $location.path(link).search(params);
                } else {// page in current spa-> Ex.
                    // 'welcome-page'
                    $location.path(link);
                }
            } else {// if(type === 'internal-link')a link in
                // current
                // page
                goToAnchor(link);
            }
        }
        if (event){
            event.preventDefault();
        }
    }


    /**
     * Selects featrue image
     * 
     * @param feature
     * @returns
     */
    function selectImage(feature) {
        return $resource.get('image')//
        .then(function(value) {
            feature.image = value;
        });
    }

    $scope.extraActions = [
        {
            title : 'Add feature',
            icon : 'add',
            action : addFeature
        },
        {
            title : 'Remove feature',
            icon : 'remove',
            action : function() {
                removeFeature(ngModel.features[ngModel.features.length - 1]);
            }
        },
        {
            title : 'Toggle layout',
            icon : 'swap_vert',
            action : function() {
                ngModel.layout = 'column';
            },
            disable : function() {
                return ngModel.layout === 'column';
            }
        },
        {
            title : 'Toggle layout',
            icon : 'swap_horiz',
            action : function() {
                ngModel.layout = 'row';
            },
            disable : function() {
                return (!ngModel.layout) || ngModel.layout === 'row';
            }
        } ];

    /*
     * Listen model
     */
    $scope.$watch('wbModel', function() {
        if (angular.isDefined($scope.wbModel)) {
            ngModel = $scope.wbModel;
            if (!angular.isDefined(ngModel.features)) {
                ngModel.features = [];
            }
        }
    });

    // Global functions
    $scope.addFeature = addFeature;
    $scope.removeFeature = removeFeature;
    $scope.removeAllFeatures = removeAllFeatures;
    $scope.editFeature = editFeature;
    $scope.moveUpFeature = moveUpFeature;
    $scope.moveDownFeature = moveDownFeature;
    $scope.runAction = runAction;
    $scope.goToAnchor = goToAnchor;
    $scope.selectImage = selectImage;

    this.test = 'hi';
    this.addFeature = addFeature;
    this.runAction = runAction;

});
