/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('am-wb-common')
/**
 * @ngdoc Controllers
 * @name AmWbCommonVideoCtrl
 * @description control the Video widget.
 * 
 * All properties will fetch from the Model such as url.
 */
.controller('AmWbCommonVideoCtrl', function($scope) {
    var keys = ['fit', 'url', 'mimeType', 'autoplay', 'loop', 'controls', 'preload', 'poster', 'description', 'label', 'keywords'];

    /**
     * Loads all parameters from model
     * 
     * @memberof AmWbCommonVideoCtrl
     */
    this.fillMedia = function() {
        for(var i = 0; i < keys.length; i++){
            var key = keys[i];
            this[key] = this.getModelProperty(key);
        }
    };


    /**
     * Initialize the widget
     * 
     * @memberof AmWbCommonVideoCtrl
     */
    this.initWidget = function(){
        var ctrl = this;
        this.on('modelUpdated', function($event){
            if(keys.indexOf($event.key) > -1){
                var key = $event.key;
                ctrl[key] = ctrl.getModelProperty(key);
            }
        });
        this.on('modelChanged', function(){
            ctrl.fillMedia();
        });
        this.fillMedia();
    };
});