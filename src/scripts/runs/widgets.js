/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('am-wb-common')
/*
 * Register modules
 */
.run(function ($widget) {
	// NOTE: audio is moved into core module
	// NOTE: video is moved into core module
	// NOTE: image is move to core module => img

	/*
	 * Default data model of featrues
	 */
	var featuresModel = {
			text: 'Header text',
			features: [{
				title: 'Title',
				text: 'This is a sample link to a site',
				icon: 'todo',
				action: {
					type: 'link',
					link: 'http://google.com'
				}
			}, {
				title: 'Title',
				text: 'This is a sample link to a site',
				icon: 'todo',
				action: {
					type: 'link',
					link: 'http://google.com'
				}
			}]
	};
	$widget.newWidget({
		type: 'CommonFeatureList',
		title: 'Feature list',
		description: 'Horizontal or vertical features list which is used to diaplay common functions of a business.',
		groups: ['commons'],
		icon: 'wb-common-link',
		// help
		help: 'https://gitlab.com/am-wb/am-wb-common/wikis/home',
		helpId: '',
		model: angular.merge({}, featuresModel),
		// functional (page)
		templateUrl: 'views/am-wb-common-widgets/feature-list.html',
		controller: 'AmWbCommonFeaturesCtrl',
		controllerAs: 'featuresCtrl',
		setting: ['common-features']
	});
	$widget.newWidget({
		type: 'CommonFeatureLinks',
		title: 'Link list',
		description: 'List of links and actions to display in row or column',
		groups: ['commons'],
		icon: 'wb-common-link',
		// help
		help: 'http://dpq.co.ir',
		helpId: '',
		model: angular.merge({}, featuresModel),
		// functional
		templateUrl: 'views/am-wb-common-widgets/feature-links.html',
		controller: 'AmWbCommonFeaturesCtrl',
		controllerAs: 'featuresCtrl',
		setting: ['common-features']
	});
	$widget.newWidget({
		// widget
		type: 'CommonFeatureMozaic',
		title: 'Mosaic list',
		description: 'Mosaic view of features',
		groups: ['commons'],
		icon: 'apps',
		// help
		help: 'http://dpq.co.ir',
		helpId: '',
		model: angular.merge({}, featuresModel),
		// functional
		templateUrl: 'views/am-wb-common-widgets/feature-mozaic.html',
		controller: 'AmWbCommonFeaturesCtrl',
		controllerAs: 'featuresCtrl',
		setting: ['common-features']
	});
	$widget.newWidget({
		// widget
		type: 'CommonActionCall',
		title: 'Action call',
		description: 'Call action with brand, title and text.',
		groups: ['commons'],
		icon: 'wb-common-link',
		// help 
		help: 'http://dpq.co.ir',
		helpId: '',
		model: angular.merge({}, featuresModel),
		// page
		templateUrl: 'views/am-wb-common-widgets/action-call.html',
		controller: 'AmWbCommonFeaturesCtrl',
		controllerAs: 'featuresCtrl',
		setting: ['SEO', 'common-features']
	});
	$widget.newWidget({
		// widget
		type: 'CommonFeatureToolbar',
		title: 'Toolbar',
		description: 'A toolbar to show actions with sidenav.',
		groups: ['commons'],
		icon: 'wb-common-toolbar',
		// help
		help: 'https://gitlab.com/weburger/am-wb-common/wikis/toolbar',
		helpId: '',
		model: angular.merge({}, featuresModel),
		// page
		templateUrl: 'views/am-wb-common-widgets/feature-toolbar.html',
		controller: 'AmWbCommonFeaturesCtrl',
		controllerAs: 'featuresCtrl',
		setting: ['SEO', 'common-features']
	});
	$widget.newWidget({
		// widget
		type: 'YoutubeVideoPlayer',
		title: 'Youtube',
		description: 'Youtube video player',
		groups: ['commons'],
		icon: 'wb-common-youtube',
		// help
		help: '', 
		helpId: 'youTube',
		// page
		templateUrl: 'views/am-wb-common-widgets/youtube-audio-player.html',
		setting: ['common-video-player', 'SEO'],
		controllerAs: 'ctrl',
		/*
		 * @ngInject
		 */
		controller: function ($wbLibs) {
			var keys = ['url', 'description', 'label', 'keywords'];
			var defaultUrl = 'https://www.youtube.com/watch?v=qb7pOyNnXUg&list=PLcbg8k6WnJMW4x_V-HPqOJ_xSdO_Vz7C5';
			/**
			 * Reduce size of the widget and return a valid height for player
			 * 
			 */
			this.getHeight = function (value) {
				if (!value) {
					return null;
				}

				var unit = value.replace(/^[-\d\.]+/, '');
				var newVal = parseFloat(value, 10) - 10;
				return newVal + unit;
			};
			/**
			 * Initialize the widget
			 * 
			 * @memberof AmWbCommonVideoCtrl
			 */
			this.initWidget = function () {
				var ctrl = this;
				function doTask($event) {
					if (keys.indexOf($event.key) > -1) {
						var key = $event.key;
						ctrl[key] = ctrl.getProperty(key) || ctrl.getModelProperty(key);
					}
				}
				/**
				 * Loads all parameters from model
				 * 
				 * @memberof AmWbCommonVideoCtrl
				 */
				this.fillMedia = function () {
					for (var i = 0; i < keys.length; i++) {
						var key = keys[i];
						this[key] = this.getModelProperty(key);
					}
					this.url = this.getModelProperty('url') || defaultUrl;
				};
				this.on('modelUpdated', function ($event) {
					doTask($event);
				});
				this.on('runtimeModelUpdated', function ($event) {
					doTask($event);
				});
				this.on('modelChanged', function () {
					this.fillMedia();
				});
				//handling 'youtube.player.ready' event and wrap it into 'success' event
				this.getScope().$on('youtube.player.ready', function (event) {
					event.source = ctrl;
					event.key = 'success';
					ctrl.fire('success', event);
				});
				//handling 'youtube.player.error' event and wrap it into 'failure' event
				this.getScope().$on('youtube.player.error', function (event) {
					event.source = ctrl;
					event.key = 'failure';
					ctrl.fire('error', event);
				});
				this.fillMedia();
			};
			$wbLibs.load('https://www.youtube.com/iframe_api');
		}
	});
	$widget.newWidget({
		// widget
		type: 'AparatVideoPlayer',
		title: 'Aparat',
		description: 'Aparat video player',
		groups: ['commons'],
		icon: 'wb-common-aparat',
		// help
		help: '',
		helpId: 'aparat',
		// page
		templateUrl: 'views/am-wb-common-widgets/aparat-audio-player.html',
		setting: ['common-video-player', 'SEO'],
		controllerAs: 'ctrl',
		/*
		 * @ngInject
		 */
		controller: function ($sce) {
			var keys = ['fit', 'url', 'description', 'label', 'keywords'];
			var defaultUrl = 'https://www.aparat.com/v/lPRVs';

			function getAparatFrameLink(murl) {
				var hcode = murl.substring(murl.lastIndexOf('/') + 1, murl.length + 1);
				var url = murl;
				if (hcode !== 'live') {
					url = 'https://www.aparat.com/video/video/embed/videohash/' + hcode + '/vt/frame';
				}
				return $sce.trustAsResourceUrl(url);
			}


			/**
			 * Loads all parameters from model
			 * 
			 * @memberof AmWbCommonVideoCtrl
			 */
			this.fillMedia = function () {
				for (var i = 0; i < keys.length; i++) {
					var key = keys[i];
					this[key] = this.getModelProperty(key);
				}
				this.frameUrl = getAparatFrameLink(this.url || defaultUrl);
			};

			/**
			 * Initialize the widget
			 * 
			 * @memberof AmWbCommonVideoCtrl
			 */
			this.initWidget = function () {
				var ctrl = this;

				// pass event to setting panel if video loaded successfully
				this.onLoad = function () {
					var event = {};
					event.source = ctrl;
					event.key = 'success';
					ctrl.fire('success', event);
				};

				// pass event to setting panel if video doesn't load successfully
				this.onError = function () {
					var event = {};
					event.source = ctrl;
					event.key = 'failure';
					ctrl.fire('error', event);
				};

				function doTask($event) {
					if (keys.indexOf($event.key) > -1) {
						var key = $event.key;
						ctrl[key] = ctrl.getProperty(key) || ctrl.getModelProperty(key);
					}
					if ($event.key === 'url') {
						ctrl.frameUrl = getAparatFrameLink(ctrl.url || defaultUrl);
					}
				}

				this.on('modelUpdated', function ($event) {
					doTask($event);
				});
				this.on('runtimeModelUpdated', function ($event) {
					doTask($event);
				});
				this.on('modelChanged', function () {
					ctrl.fillMedia();
				});
				this.fillMedia();
			};
		}
	});
	
	$widget.newWidget({
		type: 'NavBar',
		title: 'NavBar',
		description: 'Centerelized different materials about one category.',
		groups: ['commons', 'group'],
		icon: 'subject',
		// help
		help: '',
		helpId: 'nav-bar',
		// functional (page)
		templateUrl: 'views/am-wb-common-widgets/nav-bar.html',
		setting: ['nav-bar'],
		model: {
			tabs: [{
				type: 'Group',
				label: 'Tab1',
				contents:[{
					text: '<h2>This is a Text widget</h2><p>You could delete it and design the tab with your favorite widgets.</p>',
					type: 'HtmlText',
					name: 'Text',
					event: {},
					style: {
						layout: {},
						size: {},
						background: {},
						border: {},
						align: {}
					},
					version: 'wb1'
				}]
			},
			{
				type: 'Group',
				label: 'Tab2',
				contents:[{
					text: '<h2>This is a Text widget</h2><p>You could delete it and design the tab with your favorite widgets.</p>',
					type: 'HtmlText',
					name: 'Text',
					event: {},
					style: {
						layout: {},
						size: {},
						background: {},
						border: {},
						align: {}
					},
					version: 'wb1'
				}]
			}]
		}
	});
	/**
	 * @ngdoc Widgets
	 * @name Shape
	 * @description display an SVG shape
	 * 
	 * @deprecated replaced with core/svg
	 */
	$widget.newWidget({
		type: 'Shape',
		title: 'Shape',
		description: 'SVG Shape.',
		groups: ['commons'],
		icon: 'category',
		// help
		help: 'am-wb-widget-shape',
		helpId: 'am-wb-widget-shape',
		// functional (page)
		template: '',
		setting: ['shape'],
		controllerAs: 'ctrl',
		/*
		 * @ngInject
		 */
		controller: function () {
			// local variables
			this._shape;
			this._svg;
			this.mainKeys = [
				'content',
				'style.size.width',
				'style.size.height',
				'style.fill',
				'style.stroke',
				'style.strokeWidth'
				];
			this.calculateProperty = function (key, defaultValue) {
				return this.getProperty(key) || this.getModelProperty(key) || defaultValue;
			};
			this.calculateSvg = function () {
				var content = this.calculateProperty('content', '<svg width="100" height="100"><circle cx="50" cy="50" r="40" stroke="green" stroke-width="4" fill="yellow" /></svg>');
				if (!this._svg || content !== this._shape) {
					// create element
					this._shape = content;
					var shapeEle = angular.element(this._shape);
					// insert to view
					var $element = this.getElement();
					$element.empty();
					$element.append(shapeEle);
					this._svg = d3.select(shapeEle[0]);
				}
				return this._svg;
			}

			this.updateShape = function () {
				var svg = this.calculateSvg();
				var $element = this.getElement();
				// size
				svg
				.style('width', $element.width())
				.style('height', $element.height());
				// color
				svg.selectAll('path')
				.style('fill', this.calculateProperty('style.fill', 'black'))
				.style('stroke', this.calculateProperty('style.stroke', 'black'))
				.style('stroke-width', this.calculateProperty('style.strokeWidth', '1px'));
			};
			// init widget
			this.initWidget = function () {
				var ctrl = this;
				this.on('modelUpdated', function ($event) {
					var key = $event.key;
					if (ctrl.mainKeys.includes(key)) {
						ctrl.updateShape();
					}
				});
				this.on('runtimeModelUpdated', function ($event) {
					var key = $event.key;
					if (ctrl.mainKeys.includes(key)) {
						ctrl.updateShape();
					}
				});
				this.on('resize', function () {
					// TODO: maso, 2019: just update the size
					ctrl.updateShape();
				});
				this.updateShape();
			};
		}
	});
	/**
	 * @ngdoc Widgets
	 * @name Button
	 * @description A Button to do special task
	 * 
	 */
	$widget.newWidget({
		type: 'Button',
		title: 'Button',
		description: 'A Button to do special task',
		groups: ['commons'],
		icon: 'button',
		// help
		help: '',
		helpId: '',
		// functional (page)
		templateUrl: 'views/am-wb-common-widgets/button.html'
	});
	/**
	 * @ngdoc Widgets
	 * @name Import
	 * @description Import external resource into the current document
	 * 
	 */
	$widget.newWidget({
		type: 'Import',
		title: 'Import',
		description: 'Import a part of other content',
		groups: ['commons'],
		icon: 'import_export',
		setting: ['import'],
		// help
		help: '',
		helpId: '',
		// functional (page)
		templateUrl: 'views/am-wb-common-widgets/import.html',
		/*
		 * @ngInject
		 */
		controller: function ($wbUtil, $http) {

			/*
			 * NOTE: this is a utility function and can move to UTILSE
			 */
			this.findWidgetModelById = function (wbModel, id) {
				if (wbModel.id === id) {
					return wbModel;
				}
				if (wbModel.type === 'Group') {
					for (var i = 0; i < wbModel.contents.length; i++) {
						var model = this.findWidgetModelById(wbModel.contents[i], id);
						if (model) {
							return model;
						}
					}
				}
				return null;
			};
			/*
			 * NOTE: this is a utility function and can move to UTILSE
			 */
			this.downloadWidgetModel = function (url, id) {
				// TODO: maso, 2019: CHECK if URL is @configuration
				var ctrl = this;
				return $http.get(url)
				.then(function (res) {
					var totalModel = $wbUtil.clean(res.data);
					if (!id) {
						return totalModel;
					}
					// XXX: maso, 2019: find part of the models
					return ctrl.findWidgetModelById(totalModel, id);
				});
			};
			/*
			 * Load link data
			 */
			this.loadLink = function () {
				this.loading = true;
				// check if the url
				var path = this.getModelProperty('url');
				if (!path) {
					delete this.model;
					return;
				}

				// check parts
				var parts = path.split("#");
				if (!parts.length) {
					delete this.model;
					return;
				}
				//
				this.url = parts[0];
				this.id = parts.length > 1 ? parts[1] : undefined;
				var ctrl = this;
				this.downloadWidgetModel(this.url, this.id)
				.then(function (model) {
					ctrl.model = model;
					ctrl.loading = false;
				});
			};
			this.selectionEventHandler = function ($event) {
				var widget = $event.widgets[0];
				this.importedWidget = widget.getRoot();
			};
			this.getImportedWidget = function () {
				return this.importedWidget;
			};
			/*
			 * Init widget
			 */
			this.initWidget = function () {
				var ctrl = this;
				this.model = {
						type: 'Group'
				};
				this.on('modelUpdated', function ($event) {
					if ($event.key === 'url') {
						ctrl.loadLink();
					}
				});
				this.loadLink();
			};
		}
	});
});
