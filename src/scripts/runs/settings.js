/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('am-wb-common')

/**
 * Load widgets
 */
.run(function ($settings, $wbUi) {

    /*
     * Utility: change two indexs
     */
    function moveArrayItem(array, old_index, new_index) {
        if (new_index >= array.length) {
            var k = new_index - array.length;
            while ((k--) + 1) {
                array.push(undefined);
            }
        }
        array.splice(new_index, 0,
                array.splice(old_index, 1)[0]);
    }


    /**
     * @ngDoc Settings
     * @name common-features
     * @description Common feature list configurations
     */
    $settings.newPage({
        type: 'common-features',
        label: 'Items',
        icon: 'settings',
        templateUrl: 'views/am-wb-common-settings/features.html',
        description: 'Manage list of feature items',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            /*
             * Load data
             */
            this.init = function () {
                this.features = this.getProperty('features') || [];
            };
            this.removeFeature = function (index) {
                this.features.splice(index, 1);
                this.changedFeature();
            };
            this.moveUpFeature = function (index) {
                moveArrayItem(this.features, index, index - 1);
            };
            this.moveDownFeature = function (index) {
                moveArrayItem(this.features, index, index + 1);
            };
            this.addFeature = function () {
                this.features.push({
                    'icon': 'assignment_turned_in',
                    'title': 'Title',
                    'text': 'Item description.',
                    action: {
                        type: 'link',
                        link: ''
                    }
                });
                this.changedFeature();
            };
            this.editFeature = function (feature) {
                var ctrl = this;
                $wbUi.openDialog({
                    controller: 'AmWbCommonDialogmodelCtrl',
                    templateUrl: 'views/am-wb-common-dialogs/feature.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true,
                    locals: {
                        model: feature,
                        style: {
                            title: 'fetrues'
                        }
                    }
                })
                .then(function () {
                    ctrl.changedFeature();
                });
            };
            /*
             * sets new features
             */
            this.changedFeature = function () {
                this.setProperty('features', this.features);
            };
        }
    });

    /**
     * @ngDoc Settings
     * @name common-audio-player
     * @description Audio player setting page
     */
    $settings.newPage({
        type: 'common-audio-player',
        label: 'Audio Player',
        description: 'Manage playing audio in the current widget.',
        icon: 'settings',
        templateUrl: 'views/am-wb-common-settings/audio-player.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            /*
             * Load from model on model change
             */
            this.init = function () {
                this.content = this.getProperty('content') || {};
                this.setting = this.getProperty('setting') || {};
            };

            this.contentChanged = function () {
                this.setProperty('content', this.content);
            };

            this.settingChanged = function () {
                this.setProperty('setting', this.setting);
            };
        }

    });

    /**
     * @ngDoc Settings
     * @name common-video-player
     * @description Video player setting page
     */
    $settings.newPage({
        type: 'common-video-player',
        label: 'Video Player',
        description: 'Manage showing video in the current widget.',
        icon: 'settings',
        templateUrl: 'views/am-wb-common-settings/video-player.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            /*
             * Load from model on model change
             */
            this.init = function () {
                this.type = this.getProperty('type');
                this.url = this.getProperty('url');
                this.mimeType = this.getProperty('mimeType');

                this.preload = this.getProperty('preload');
                this.poster = this.getProperty('poster');
                this.fit = this.getProperty('fit');

                this.autoplay = this.getProperty('autoplay');
                this.controls = this.getProperty('controls');
                this.loop = this.getProperty('loop');
            };

            this.updateFit = function () {
                this.setProperty('fit', this.fit);
            };
            this.updatePoster = function () {
                this.setProperty('poster', this.poster);
            };
            this.updatePreload = function () {
                this.setProperty('preload', this.preload);
            };

            this.updateMimeType = function () {
                this.setProperty('mimeType', this.mimeType);
            };
            this.updateUrl = function () {
                this.setProperty('url', this.url);
            };

            this.updateAutoplay = function () {
                this.setProperty('autoplay', this.autoplay);
            };
            this.updateControls = function () {
                this.setProperty('controls', this.controls);
            };
            this.updateLoop = function () {
                this.setProperty('loop', this.loop);
            };
        }
    });

    /**
     * @ngDoc Settings
     * @name amh-common-image
     * @description Image setting page
     */
    $settings.newPage({
        type: 'amh-common-image',
        label: 'Image',
        description: 'Manage showing image in the current widget.',
        icon: 'settings',
        templateUrl: 'views/am-wb-common-settings/image.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            /*
             * Load from model on model change
             */
            this.init = function () {
                this.url = this.getProperty('url');
                this.fit = this.getProperty('fit');
            };

            this.updateUrl = function () {
                this.setProperty('url', this.url);
            };
            this.updateFit = function () {
                this.setProperty('fit', this.fit);
            };
        }
    });

    /**
     * @ngDoc Settings
     * @name nav-bar
     * @description Link setting page
     */
    $settings.newPage({
        type: 'nav-bar',
        label: 'Navigation bar',
        description: 'Navigation through tabs.',
        icon: 'subject',
        templateUrl: 'views/am-wb-common-settings/navigation-bar.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            // Check data model
            this.init = function () {
                this.tabs = this.getProperty('tabs') || [];
            };

            this.deleteTab = function (index) {
                this.tabs.splice(index, 1);
                this.tabsChanged();
            };

            this.addTab = function () {
                this.tabs.push({
                    'type': 'Group',
                    'label': 'Title',
                    'contents': []
                });
                this.tabsChanged();
            };

            this.tabsChanged = function () {
                this.setProperty('tabs', this.tabs);
            };
        }
    });

    /**
     * @ngdoc Widget setting
     * @name shape
     * @description Manages shape settings
     */
    $settings.newPage({
        type: 'shape',
        label: 'Shape',
        description: 'Manage shape settings.',
        icon: 'format_shapes',
        templateUrl: 'views/am-wb-common-settings/shape.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function () {
            this.init = function () {
                this.fill = this.getStyle('fill');
                this.stroke = this.getStyle('stroke');
                this.strokeWidth = this.getStyle('strokeWidth');
                this.content = this.getProperty('content');
            };

            this.styleChanged = function (key, value) {
                this.setStyle(key, value);
            };

            this.contentChanged = function (content) {
                this.setProperty('content', content);
            };
        }
    });

    /**
     * @ngdoc Widget setting
     * @name import
     * @description Import setting
     */
    $settings.newPage({
        type: 'import',
        label: 'Import',
        description: 'Settings of import.',
        icon: 'format_shapes',
        templateUrl: 'views/am-wb-common-settings/import.html',
        controllerAs: 'ctrl',
        /*
         * @ngInject
         */
        controller: function ($cms) {
            this.init = function () {
                this.url = this.getProperty('url');
            };

            this.urlChanged = function () {
                this.setProperty('url', this.url);
                this.checkForCanonicalLink(this.url);
            };

            /*
             * Check the content which the imported widget comes from 
             * to see if the content has had a canonical link. If so, it 
             * set canonicalLink in view.
             */
            this.checkForCanonicalLink = function (url) {
                if (this.canonicalLink) {
                    this.canonicalLink = null;
                }
                var ctrl = this;
                var res = url.match('/api\/v2\/cms\/contents\/([^/]+)\/content#.*');
                if (res) {
                    //res[1] is equal to every thing inside ([^/]+) which is the 'id' of content
                    this.id = res[1];
                    $cms.getContent(this.id, {'graphql': '{metas{key,value}}'})
                    .then(function (res) {
                        for (var i = 0; i < res.metas.length; i++) {
                            if (res.metas[i].key === 'link.canonical') {
                                ctrl.canonicalLink = res.metas[i].value;
                            }
                        }
                    }, function () {
                        //TODO: Masood, 2019: handle error
                    });
                } 
            };
        }
    });
});


