/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-common')

/**
 * @ngdoc Directives
 * @name wbPreventReload
 * @param {element}
 *            element
 * @description prevent the element to do a default/predefined work on its
 *              click. This diretive: - Acts as an attribute - Get an element as
 *              input and define a function on its 'click'. - Disables the
 *              default functionality of 'element' and allows us to handle it in
 *              other ways.
 * 
 * Why is this directive important? Example:
 * 
 * Sometimes we need to both 'ng-href'(for better performance when the search
 * engines are searching) and 'ng-click'(to do other thing)in a button's
 * attributes . But we want to disable 'ng-href' and allow the 'ng-click' to
 * handle the situation. If we use 'ng-href' for example in a 'Button' to go to
 * a link and click on the button then the explorer will go to that link
 * automatically. Now, if we want to prevent explorer to do this, we could use
 * this directive as 'attribute' in button's attributes and handle the button
 * 'onClick' in another way.
 * 
 * @author masood<masoodzarei64@gamil.com>
 */
.directive('wbPreventReload', function() {
	return {
		restrict : 'A',
		link : function(scope, element) {
			element.on('click', function(event) {
				event.preventDefault();
			});
		}
	};
});
