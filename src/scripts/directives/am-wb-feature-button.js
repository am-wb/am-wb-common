/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('am-wb-common')

/**
 * @ngdoc Directives
 * @name am-wb-feature-button
 * @description Show a feature in button with action
 * 
 * This is used in toolbar
 */
.directive('amWbFeatureButton', function($q, $parse, $resource, $anchorScroll, $location) {

	/**
	 * Link data and view
	 */
	function postLink(scope, attr, elem, ngModel) {
		var ctrl = {};
		
		function goToAnchor(anchor){
			if ($location.hash() !== anchor) {
				// set the $location.hash to `anchor` and
				// $anchorScroll will automatically scroll to it
				$location.hash(anchor);
			} else {
				// call $anchorScroll() explicitly,
				// since $location.hash hasn't changed
				$anchorScroll();
			}
		}

		/*
		 * Load the feature
		 */
		function loadFeatrue(feature){
			scope.feature = feature;
			if(!feature.action){
				return;
			}
			switch(feature.action.type) {
			case 'link':
			case 'internal-link':
				ctrl.type = feature.action.type;
				ctrl.link = feature.action.link;
				break;
			default:
				ctrl.type = 'unknown';
			}
		}

		/*
		 * Run the current feature
		 */
		scope.runAction = function(){
			// TODO: maso, 2018: handle other types
			if(ctrl.type === 'internal-link'){
				goToAnchor(ctrl.link);
			}
		};
		
		// Watch model
		scope.$watch(function(){
			return ngModel.$modelValue;
		}, loadFeatrue, true);
		scope.featue = ngModel.$viewValue;
		scope.ctrl = ctrl;
	}

	return {
		restrict : 'E',
		transclude : true,
		replace: true,
		require: '^ngModel',
		scope: {
			wbIconButton: '<?'
		},
		templateUrl : 'views/directives/am-wb-feature-button.html',
		link: postLink,
		controller: function ($scope, $rootScope) {
		    $rootScope.$watch('editable', function (val) {
			$scope.editable = val;
		    });
		}
	};
});
